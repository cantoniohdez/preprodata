# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'scale.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui
from sklearn import preprocessing
import numpy as np
from PyQt4.QtGui import *
import sys, os
import manage_database
try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)



class Ui_FormScale(object):
    dataset = np.array([])
    Form = object

    def __init__(self):
        self.dataset = manage_database.obtain_dataset()



    def setupUi(self, Form):

        self.Form = Form
        self.Form.setObjectName(_fromUtf8("Form"))
        self.Form.resize(400, 145)

        self.frame = QtGui.QFrame(Form)
        self.frame.setGeometry(QtCore.QRect(10, 10, 381, 81))
        self.frame.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtGui.QFrame.Raised)
        self.frame.setObjectName(_fromUtf8("frame"))

        self.lineEdit = QtGui.QLineEdit(self.frame)
        self.lineEdit.setGeometry(QtCore.QRect(30, 30, 113, 27))
        self.lineEdit.setObjectName(_fromUtf8("lineEdit"))

        self.label = QtGui.QLabel(self.frame)
        self.label.setGeometry(QtCore.QRect(30, 10, 81, 17))
        self.label.setObjectName(_fromUtf8("label"))

        self.lineEdit_2 = QtGui.QLineEdit(self.frame)
        self.lineEdit_2.setGeometry(QtCore.QRect(220, 30, 113, 27))
        self.lineEdit_2.setObjectName(_fromUtf8("lineEdit_2"))

        self.label_2 = QtGui.QLabel(self.frame)
        self.label_2.setGeometry(QtCore.QRect(220, 10, 81, 17))
        self.label_2.setObjectName(_fromUtf8("label_2"))

        self.frame_2 = QtGui.QFrame(Form)
        self.frame_2.setGeometry(QtCore.QRect(10, 100, 381, 41))
        self.frame_2.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame_2.setFrameShadow(QtGui.QFrame.Raised)
        self.frame_2.setLineWidth(2)
        self.frame_2.setObjectName(_fromUtf8("frame_2"))

        self.pushButton_2 = QtGui.QPushButton(self.frame_2)
        self.pushButton_2.setGeometry(QtCore.QRect(10, 10, 99, 27))
        self.pushButton_2.setObjectName(_fromUtf8("pushButton_2"),)
        self.pushButton_2.clicked.connect(Form.close)
        self.pushButton = QtGui.QPushButton(self.frame_2)
        self.pushButton.setGeometry(QtCore.QRect(270, 10, 99, 27))
        self.pushButton.setObjectName(_fromUtf8("pushButton"))
        self.pushButton.clicked.connect(self.scale_range)

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        Form.setWindowTitle(_translate("Form", "Scale", None))
        self.label.setText(_translate("Form", "Min Value", None))
        self.label_2.setText(_translate("Form", "Max Value", None))
        self.pushButton_2.setText(_translate("Form", "Cancel", None))
        self.pushButton.setText(_translate("Form", "Scale", None))

    def scale_range(self):
        try:
            new_min = float(self.lineEdit.text())
            new_max = float(self.lineEdit_2.text())
            if new_min > new_max:
                self.msg("Min Value must be less than Max Value")
            else:
                list_column_numerics = manage_database.obtain_numeric_columns()
                for x in list_column_numerics:
                    xx = np.array(self.dataset.T[int(x)]).astype(np.float)
                    mylistTemp = []
                    max = np.amin(xx)
                    min = np.amax(xx)
                    for y in range(0, self.dataset.shape[0]):
                        myvalue = xx[y]
                        mynewvalue = (((myvalue - min) * (new_max - new_min))/(max - min))+new_min

                        if np.isnan(mynewvalue):
                            mylistTemp.append(0)
                        else:
                            mylistTemp.append(mynewvalue)
                    # Retorno los valores escalados en el rango definido
                    # Elimino la columna que voy a escalar del dataset Orginal
                    xx = np.delete(self.dataset, int(x), axis=1)
                    # Convierto a lista para usar la funcion insert de las lista
                    yy = xx.T.tolist()
                    # Inserto la columna escalada en el dataset al que ya le elimine esa columna
                    yy.insert(int(x), np.array(mylistTemp))
                    # ILe asigno el dataset con la modificacion al dataset global
                    self.dataset = np.array(yy).T
                manage_database.insert_dataset(self.dataset)
                manage_database.msg("Successful Operation")
                self.Form.close()
        except Exception as e:
            print(str(e))
            manage_database.msg("The input is empty or the values are not numerics")

if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    Form = QtGui.QWidget()
    ui = Ui_FormScale()
    ui.setupUi(Form)
    Form.show()
    sys.exit(app.exec_())

