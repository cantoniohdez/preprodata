# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'pearson_correlation.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui
from PyQt4.QtGui import *
import pearson_correlation
import manage_database
import numpy as np
import manage_file as mf
import complementary_functions as cf

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_FormPearsonCorrelation(object):
    def __init__(self):
        self.dataset = manage_database.obtain_dataset()
        self.flag = False

    def setupUi(self, Form):
        Form.setObjectName(_fromUtf8("Form"))
        Form.resize(578, 300)
        self.frame = QtGui.QFrame(Form)
        self.frame.setGeometry(QtCore.QRect(10, 10, 381, 221))
        self.frame.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtGui.QFrame.Raised)
        self.frame.setObjectName(_fromUtf8("frame"))
        self.tableWidget = QtGui.QTableWidget(self.frame)
        self.tableWidget.setGeometry(QtCore.QRect(0, 0, 381, 221))
        self.tableWidget.setObjectName(_fromUtf8("tableWidget"))
        self.tableWidget.setColumnCount(0)
        self.tableWidget.setRowCount(0)
        self.frame_2 = QtGui.QFrame(Form)
        self.frame_2.setGeometry(QtCore.QRect(10, 240, 561, 51))
        self.frame_2.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame_2.setFrameShadow(QtGui.QFrame.Raised)
        self.frame_2.setObjectName(_fromUtf8("frame_2"))
        self.pushButton = QtGui.QPushButton(self.frame_2)
        self.pushButton.setGeometry(QtCore.QRect(270, 10, 99, 27))
        self.pushButton.setObjectName(_fromUtf8("pushButton"))
        self.pushButton.clicked.connect(self.delete_selected_relation)
        self.pushButton_2 = QtGui.QPushButton(self.frame_2)
        self.pushButton_2.setGeometry(QtCore.QRect(10, 10, 99, 27))
        self.pushButton_2.setObjectName(_fromUtf8("pushButton_2"))
        self.pushButton_2.clicked.connect(Form.close)
        self.pushButton_4 = QtGui.QPushButton(self.frame_2)
        self.pushButton_4.setGeometry(QtCore.QRect(440, 10, 99, 27))
        self.pushButton_4.setObjectName(_fromUtf8("pushButton_4"))
        self.pushButton_4.clicked.connect(self.saveFile)
        self.frame_3 = QtGui.QFrame(Form)
        self.frame_3.setGeometry(QtCore.QRect(400, 10, 171, 221))
        self.frame_3.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame_3.setFrameShadow(QtGui.QFrame.Raised)
        self.frame_3.setObjectName(_fromUtf8("frame_3"))
        self.label = QtGui.QLabel(self.frame_3)
        self.label.setGeometry(QtCore.QRect(50, 10, 68, 17))
        self.label.setObjectName(_fromUtf8("label"))
        self.doubleSpinBox = QtGui.QDoubleSpinBox(self.frame_3)
        self.doubleSpinBox.setGeometry(QtCore.QRect(10, 40, 151, 27))
        self.doubleSpinBox.setMinimum(0.0)
        self.doubleSpinBox.setMaximum(1.0)
        self.doubleSpinBox.setSingleStep(0.01)
        self.doubleSpinBox.setProperty("value", 0.9)
        self.doubleSpinBox.setObjectName(_fromUtf8("doubleSpinBox"))
        self.pushButton_3 = QtGui.QPushButton(self.frame_3)
        self.pushButton_3.setGeometry(QtCore.QRect(40, 90, 99, 27))
        self.pushButton_3.setObjectName(_fromUtf8("pushButton_3"))
        self.pushButton_3.clicked.connect(self.clear_function)

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)
        self.np_matriz_correlation = pearson_correlation.pearson_correlations(self.dataset.T, 0)
        name_column = manage_database.obtain_name_column(0).tolist()
        self.tableWidget.setColumnCount(self.np_matriz_correlation.shape[1])  # rows and columns of table
        self.tableWidget.setRowCount(self.np_matriz_correlation.shape[0])
        # self.tableWidget.setItem(1, 1, QTableWidgetItem(self.dataset[0][0]))
        self.tableWidget.setHorizontalHeaderLabels(name_column)
        self.tableWidget.setVerticalHeaderLabels(name_column)
        for row in range(0, self.np_matriz_correlation.shape[0]):  # add items from array to QTableWidget
            for column in range(0, self.np_matriz_correlation.shape[1]):
                self.tableWidget.setItem(row, column, QTableWidgetItem(str(self.np_matriz_correlation[row][column])))

    def retranslateUi(self, Form):
        Form.setWindowTitle(_translate("Form", "Pearson Correlation", None))
        self.pushButton.setText(_translate("Form", "Delete", None))
        self.pushButton_2.setText(_translate("Form", "Close", None))
        self.pushButton_4.setText(_translate("Form", "Save", None))
        self.label.setText(_translate("Form", "Umbral", None))
        self.pushButton_3.setText(_translate("Form", "Clean", None))

    def delete_selected_relation(self):
        self.flag = True
        number_column = self.tableWidget.currentColumn()
        listComplete = []

        structure = manage_database.obtain_structure()

        structure = np.delete(structure, number_column, 0)
        structure = np.delete(structure, 0, 1)
        manage_database.deleteTableStructure()

        for x in range(0, structure.shape[0]):
            field_name = structure[x, 0]
            field_type = structure[x, 1]
            field_len = structure[x, 2]
            field_len_dec = structure[x, 3]
            objTemp = manage_database.Metadatos(field_name, field_type, field_len, field_len_dec)
            listComplete.append(objTemp)
        manage_database.insert_metadatos(listComplete)

        self.dataset = np.delete(self.dataset, number_column, 1)
        manage_database.insert_dataset(self.dataset)

        self.np_matriz_correlation = np.delete(self.np_matriz_correlation, number_column, 0)
        self.np_matriz_correlation = np.delete(self.np_matriz_correlation, number_column, 1)
        name_column = manage_database.obtain_name_column(0).tolist()
        self.tableWidget.setColumnCount(self.np_matriz_correlation.shape[1])  # rows and columns of table
        self.tableWidget.setRowCount(self.np_matriz_correlation.shape[0])
        #self.tableWidget.setItem(1, 1, QTableWidgetItem(self.dataset[0][0]))
        self.tableWidget.setHorizontalHeaderLabels(name_column)
        self.tableWidget.setVerticalHeaderLabels(name_column)
        for row in range(0, self.np_matriz_correlation.shape[0]):  # add items from array to QTableWidget
            for column in range(0, self.np_matriz_correlation.shape[1]):
                self.tableWidget.setItem(row, column, QTableWidgetItem(str(self.np_matriz_correlation[row][column])))

    def clear_function(self):
        umbral = float(self.doubleSpinBox.text())
        for row in range(0, self.np_matriz_correlation.shape[0]):  # add items from array to QTableWidget
            for column in range(0, self.np_matriz_correlation.shape[1]):
                if float(self.np_matriz_correlation[row][column])>= umbral:
                    self.tableWidget.setItem(row, column, QTableWidgetItem("X"))
                else:
                    self.tableWidget.setItem(row, column, QTableWidgetItem(""))

    def saveFile(self):
        if self.flag:
            dir_save = QFileDialog().getSaveFileName()
            mf.savefile(dir_save, self.dataset)
        else:
            cf.msg("First delete some variable")


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    Form = QtGui.QWidget()
    ui = Ui_FormPearsonCorrelation()
    ui.setupUi(Form)
    Form.show()
    sys.exit(app.exec_())

