# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'statistical.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui
import numpy as np
from PyQt4 import QtGui
from PyQt4.QtGui import *
import matplotlib.pyplot as plt

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_FormStatistical(object):

    def __init__(self,data):
        self.data_column = []

        self.numberofvalues = 0
        self.numberofMissingValues = 0

        for x in data:
            self.numberofvalues+=1
            value = x.text()
            try:
                if value == "NaN":
                    self.numberofMissingValues+=1
                else:
                    self.data_column.append(float(value))

            except Exception:
                self.msg("There are not numeric values")

        self.list_categories = np.unique(self.data_column)
        self.count_categories = self.list_categories.shape[0]
        self.count_by_categories = np.zeros(self.count_categories)

        for y in self.data_column:
            for z in range(0,len(self.list_categories)):
                if str(y) == str(self.list_categories[z]):
                    temp = 0
                    temp = self.count_by_categories[z]
                    self.count_by_categories[z] = temp+1
        #print("OKKKKKK")
        #print(self.count_by_categories)




    def setupUi(self, Form):
        Form.setObjectName(_fromUtf8("Form"))
        Form.resize(349, 388)
        self.frame = QtGui.QFrame(Form)
        self.frame.setGeometry(QtCore.QRect(10, 10, 331, 301))
        self.frame.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtGui.QFrame.Raised)
        self.frame.setObjectName(_fromUtf8("frame"))
        self.tableWidget = QtGui.QTableWidget(self.frame)
        self.tableWidget.setGeometry(QtCore.QRect(10, 10, 311, 281))
        self.tableWidget.setObjectName(_fromUtf8("tableWidget"))
        self.tableWidget.setColumnCount(1)
        self.tableWidget.setRowCount(self.count_categories +3)
        item = QtGui.QTableWidgetItem()
        self.tableWidget.setVerticalHeaderItem(0, item)
        item = QtGui.QTableWidgetItem()
        self.tableWidget.setVerticalHeaderItem(1, item)
        item = QtGui.QTableWidgetItem()
        self.tableWidget.setVerticalHeaderItem(2, item)
        item = QtGui.QTableWidgetItem()
        for v in range(0, self.count_categories):
            self.tableWidget.setVerticalHeaderItem(3+v, item)
            item = QtGui.QTableWidgetItem()

        self.tableWidget.setHorizontalHeaderItem(0, item)
        self.frame_2 = QtGui.QFrame(Form)
        self.frame_2.setGeometry(QtCore.QRect(10, 330, 331, 51))
        self.frame_2.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame_2.setFrameShadow(QtGui.QFrame.Raised)
        self.frame_2.setObjectName(_fromUtf8("frame_2"))
        self.pushButton = QtGui.QPushButton(self.frame_2)
        self.pushButton.setGeometry(QtCore.QRect(220, 10, 99, 27))
        self.pushButton.setObjectName(_fromUtf8("pushButton"))
        self.pushButton.clicked.connect(Form.close)

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

        self.tableWidget.setItem(1, -1, QTableWidgetItem(str(self.numberofvalues)))
        self.tableWidget.setItem(1, 0, QTableWidgetItem(str(self.count_categories)))
        self.tableWidget.setItem(1, 1, QTableWidgetItem(str(self.numberofMissingValues)))
        for v in range(0, self.count_categories):
            self.tableWidget.setItem(1, v+2, QTableWidgetItem(str(self.count_by_categories[v])))



    def retranslateUi(self, Form):
        Form.setWindowTitle(_translate("Form", "Statistics", None))
        item = self.tableWidget.verticalHeaderItem(0)
        item.setText(_translate("Form", "Count of values", None))
        item = self.tableWidget.verticalHeaderItem(1)
        item.setText(_translate("Form", "Count of Categories", None))
        item = self.tableWidget.verticalHeaderItem(2)
        item.setText(_translate("Form", "Number of Missing Values", None))
        for v in range(0, len(self.list_categories)):
            number = int(3+int(v))
            #print(type(number))
            #print(type(v))
            item = self.tableWidget.verticalHeaderItem(v+3)
            item.setText(_translate("Form", "Count of values with categorie: "+str(self.list_categories[v]), None))

        item = self.tableWidget.horizontalHeaderItem(0)
        item.setText(_translate("Form", "Selection", None))
        self.pushButton.setText(_translate("Form", "Close", None))

    def msg(self, text):
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Information)
        msg.setText(text)
        # msg.setInformativeText("This is additional information")
        msg.setWindowTitle("Information")
        msg.setDetailedText("The details are as follows:")
        msg.setStandardButtons(QMessageBox.Ok)
        msg.exec_()

if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    Form = QtGui.QWidget()
    ui = Ui_FormStatistical()
    ui.setupUi(Form)
    Form.show()
    sys.exit(app.exec_())

