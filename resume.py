# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'resume.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui
import numpy as np
from PyQt4.QtGui import *
import numpy as np
from PyQt4 import QtGui
import manage_database
import complementary_functions as cf


try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_FormResume(object):
    dataset = np.array([])

    def __init__(self, dataset):
        self.dataset = np.array(dataset)

    def setupUi(self, Form):
        Form.setObjectName(_fromUtf8("Form"))
        Form.resize(400, 300)
        self.tableWidget = QtGui.QTableWidget(Form)
        self.tableWidget.setGeometry(QtCore.QRect(0, 0, 401, 301))
        self.tableWidget.setObjectName(_fromUtf8("tableWidget"))
        self.tableWidget.setColumnCount(0)
        self.tableWidget.setRowCount(0)

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

        if self.dataset.shape[0] != 0:
            name_column = manage_database.obtain_name_column(0).tolist()
            if len(name_column) == self.dataset.shape[0]:

                self.tableWidget.setColumnCount(self.dataset.shape[1])  # rows and columns of table
                self.tableWidget.setRowCount(self.dataset.shape[0])
                #self.tableWidget.setItem(1, 1, QTableWidgetItem(self.dataset[0][0]))
                self.tableWidget.setVerticalHeaderLabels(name_column)
                self.tableWidget.setHorizontalHeaderLabels(["Total Entropy","Entropy","Sample Size" ])
                for row in range(0, self.dataset.shape[0]):  # add items from array to QTableWidget
                    for column in range(0, self.dataset.shape[1]):
                        self.tableWidget.setItem(row, column, QTableWidgetItem(str(self.dataset[row][column])))
            else:
                cf.msg("The dataset and the defined structure are not equal")
        else:
            cf.msg("La base de datos esta vacia")

    def retranslateUi(self, Form):
        Form.setWindowTitle(_translate("Form", "Resume", None))

