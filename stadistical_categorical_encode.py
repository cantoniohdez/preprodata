# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'stadistical_categorical_encode.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui
from PyQt4.QtGui import *
import numpy as np
from PyQt4 import QtGui
import pearson_correlation
import manage_database
import random

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_FormStadistical_Categorical_Encode(object):

    def setupUi(self, Form):
        Form.setObjectName(_fromUtf8("Form"))
        Form.resize(661, 708)
        self.frame = QtGui.QFrame(Form)
        self.frame.setGeometry(QtCore.QRect(10, 10, 641, 231))
        self.frame.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtGui.QFrame.Raised)
        self.frame.setObjectName(_fromUtf8("frame"))
        self.label = QtGui.QLabel(self.frame)
        self.label.setGeometry(QtCore.QRect(20, 40, 231, 17))
        self.label.setObjectName(_fromUtf8("label"))
        self.label_2 = QtGui.QLabel(self.frame)
        self.label_2.setGeometry(QtCore.QRect(60, 80, 191, 17))
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.label_3 = QtGui.QLabel(self.frame)
        self.label_3.setGeometry(QtCore.QRect(150, 120, 101, 21))
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.label_4 = QtGui.QLabel(self.frame)
        self.label_4.setGeometry(QtCore.QRect(240, 10, 141, 17))
        self.label_4.setObjectName(_fromUtf8("label_4"))
        self.lineEdit = QtGui.QLineEdit(self.frame)
        self.lineEdit.setGeometry(QtCore.QRect(270, 40, 113, 27))
        self.lineEdit.setObjectName(_fromUtf8("lineEdit"))
        self.lineEdit_2 = QtGui.QLineEdit(self.frame)
        self.lineEdit_2.setGeometry(QtCore.QRect(270, 80, 113, 27))
        self.lineEdit_2.setObjectName(_fromUtf8("lineEdit_2"))
        self.lineEdit_3 = QtGui.QLineEdit(self.frame)
        self.lineEdit_3.setGeometry(QtCore.QRect(270, 120, 113, 27))
        self.lineEdit_3.setObjectName(_fromUtf8("lineEdit_3"))
        self.label_5 = QtGui.QLabel(self.frame)
        self.label_5.setGeometry(QtCore.QRect(390, 40, 171, 17))
        self.label_5.setObjectName(_fromUtf8("label_5"))
        self.pushButton = QtGui.QPushButton(self.frame)
        self.pushButton.setGeometry(QtCore.QRect(500, 190, 99, 27))
        self.pushButton.setObjectName(_fromUtf8("pushButton"))
        self.pushButton.clicked.connect(self.encode)
        self.pushButton_2 = QtGui.QPushButton(self.frame)
        self.pushButton_2.setGeometry(QtCore.QRect(40, 190, 99, 27))
        self.pushButton_2.setObjectName(_fromUtf8("pushButton_2"))
        self.pushButton_2.clicked.connect(Form.close)

        self.tableWidget_1 = QtGui.QTableWidget(Form)
        self.tableWidget_1.setGeometry(QtCore.QRect(10, 280, 641, 161))
        self.tableWidget_1.setObjectName(_fromUtf8("tableWidget_1"))
        self.tableWidget_1.setColumnCount(1)
        self.tableWidget_1.setRowCount(0)

        self.label_6 = QtGui.QLabel(Form)
        self.label_6.setGeometry(QtCore.QRect(20, 260, 241, 16))
        self.label_6.setObjectName(_fromUtf8("label_6"))
        self.label_7 = QtGui.QLabel(Form)
        self.label_7.setGeometry(QtCore.QRect(20, 450, 141, 17))
        self.label_7.setObjectName(_fromUtf8("label_7"))


        self.tableWidget = QtGui.QTableWidget(Form)
        self.tableWidget.setGeometry(QtCore.QRect(10, 470, 641, 211))
        self.tableWidget.setObjectName(_fromUtf8("tableWidget"))
        self.tableWidget.setColumnCount(0)
        self.tableWidget.setRowCount(0)
        """
        self.label_8 = QtGui.QLabel(Form)
        self.label_8.setGeometry(QtCore.QRect(20, 685, 141, 17))
        self.label_8.setObjectName(_fromUtf8("label_7"))
        """
        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)
        regexp = QtCore.QRegExp('^([01]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])$')
        validator = QtGui.QRegExpValidator(regexp)
        self.lineEdit_2.setValidator(validator)

        regexp1 = QtCore.QRegExp('^[2-3]|2[2-9]|3[0-6]$')
        validator = QtGui.QRegExpValidator(regexp1)
        self.lineEdit.setValidator(validator)

        regexp2 = QtCore.QRegExp('^[0-9][0-9]|10[0-0]$')
        validator = QtGui.QRegExpValidator(regexp2)
        self.lineEdit_3.setValidator(validator)

        self.progressBar = QtGui.QProgressBar(Form)
        self.progressBar.setGeometry(QtCore.QRect(20, 685, 400, 17))
        self.progressBar.setProperty("value", 0)
        self.progressBar.setObjectName(_fromUtf8("progressBar"))

    def retranslateUi(self, Form):
        Form.setWindowTitle(_translate("Form", "Stadistical Categorical Encoding", None))
        self.label.setText(_translate("Form", "Number of Samples in each mean", None))
        self.label_2.setText(_translate("Form", "Categories for chi-cuadrado", None))
        self.label_3.setText(_translate("Form", "Security Factor", None))
        self.label_4.setText(_translate("Form", "Input of algorithm", None))
        self.lineEdit.setText(_translate("Form", "36", None))
        self.lineEdit_2.setText(_translate("Form", "10", None))
        self.lineEdit_3.setText(_translate("Form", "2", None))
        self.label_5.setText(_translate("Form", "Values in Range: [22,36]", None))
        self.pushButton.setText(_translate("Form", "Encode", None))
        self.pushButton_2.setText(_translate("Form", "Cancel", None))
        self.label_6.setText(_translate("Form", "Values Resulting from coding", None))
        self.label_7.setText(_translate("Form", "Correlation Analysis", None))
        #self.label_8.setText(_translate("Form", "Stage {0}/{1}".format(0, 0), None))
        Form.setWindowTitle(_translate("Form", "Form", None))

    def encode(self):
        number_sample = int(self.lineEdit.text())
        chi_cuadrado = int(self.lineEdit_2.text())
        security_factor = int(self.lineEdit_3.text())
        matriz_correlation, info_text_area = self.catEstadisticalEncoding(number_sample,chi_cuadrado,security_factor)

        np_matriz_correlation = np.array(matriz_correlation)

        name_column = manage_database.obtain_name_column(0).tolist()
        self.tableWidget.setColumnCount(np_matriz_correlation.shape[1])  # rows and columns of table
        self.tableWidget.setRowCount(np_matriz_correlation.shape[0])
        #self.tableWidget.setItem(1, 1, QTableWidgetItem(self.dataset[0][0]))
        self.tableWidget.setHorizontalHeaderLabels(name_column)
        self.tableWidget.setVerticalHeaderLabels(name_column)
        for row in range(0, np_matriz_correlation.shape[0]):  # add items from array to QTableWidget
            for column in range(0, np_matriz_correlation.shape[1]):
                self.tableWidget.setItem(row, column, QTableWidgetItem(str(np_matriz_correlation[row][column])))

        self.tableWidget_1.setRowCount(len(info_text_area))
        self.tableWidget_1.setHorizontalHeaderLabels(["                            Information                               "])
        for x in range(0, len(info_text_area)):
            self.tableWidget_1.setItem(x, 0, QTableWidgetItem(str(info_text_area[x])))


    # Algoritmo para asignar valores numericos a las categorias
    # Parametros
    # 1- Dataset - Conjunto de datos.
    # 2- listCat - Lista que contiene la posicion de las variables categoricas.
    def catEstadisticalEncoding(self,muestras_en_cada_media, chi_cuadrado, factor_seguridad):

        dataset = manage_database.obtain_dataset()
        listCategories_var = manage_database.obtain_categorical_columns()
        resumen_of_result = []
        count = 0
        cantidad_iteraciones = int(5 * chi_cuadrado * factor_seguridad)
        for position_column in listCategories_var:
            promedio = []
            datasetTemp = dataset
            historial_of_value_correlation_for_end = []
            #print("Total de Iteraciones {0}".format(cantidad_iteraciones))
            for value1 in range(0, cantidad_iteraciones):
                count=count + 1
                #print("Iteracion {0}".format(value1))
                self.progressBar.setProperty("value", (count/(cantidad_iteraciones*len(listCategories_var)))*100)
                historial_of_value_correlation_for_var = []
                for value2 in range(0, muestras_en_cada_media):
                 # Obtengo los valores de las columnas categoricas
                    columnDataset = datasetTemp[:, int(position_column)]

                    # Obtengo los valores que toma la variable categorica
                    listCategories = pearson_correlation.delete_duplicados(columnDataset)

                    listCategoriesRandom = pearson_correlation.generate_values_categories(listCategories)

                    columnDataset_Random = pearson_correlation.change_Categories_to_Categories_Random(columnDataset, listCategories,
                                                                                  listCategoriesRandom)

                    datasetNew = pearson_correlation.add_column_random_to_dataset(datasetTemp, columnDataset_Random, position_column)

                    column_selected = pearson_correlation.myrandomInt(datasetNew.shape[1], int(position_column))

                    if pearson_correlation.isCategorical(column_selected, listCategories_var) == True:
                        # Obtengo los valores de las columnas categoricas
                        columnDataset2 = datasetTemp[:, column_selected]

                        # Obtengo los valores que toma la variable categorica
                        listCategories2 = pearson_correlation.delete_duplicados(columnDataset2)

                        listCategoriesRandom2 = pearson_correlation.generate_values_categories(listCategories2)

                        columnDataset_Random2 = pearson_correlation.change_Categories_to_Categories_Random(columnDataset2, listCategories2,
                                                                                       listCategoriesRandom2)

                        datasetNew2 = pearson_correlation.add_column_random_to_dataset(datasetNew, columnDataset_Random2, column_selected)

                        value_correlation = pearson_correlation.value_pearson_correlation(np.array(columnDataset_Random),
                                                                      np.array(datasetNew2[:, column_selected]))


                        objectTemp = pearson_correlation.Correlation_Values(listCategories, listCategoriesRandom, value_correlation)
                        historial_of_value_correlation_for_var.append(objectTemp)


                    else:

                        value_correlation = pearson_correlation.value_pearson_correlation(np.array(columnDataset_Random),
                                                                      np.array(datasetTemp[:, column_selected]))

                        objectTemp = pearson_correlation.Correlation_Values(listCategories, listCategoriesRandom, value_correlation)
                        historial_of_value_correlation_for_var.append(objectTemp)

                # Busco el maximo valor de las correlaciones y entonces sustituyo los valores de las
                # categorias de la variable por las categorias aleatorias que resultaron tener mayor
                # valor de correlacion
                max_value_of_correlation = pearson_correlation.search_max_value_of_correlation(historial_of_value_correlation_for_var)
                datasetTemp = np.array(
                    pearson_correlation.replace_column_dataset_with_value_correlation_max(datasetTemp, int(position_column),
                                                                      max_value_of_correlation.value_of_categories_original,
                                                                      max_value_of_correlation.value_random_of_categories))
                objectTempEnd = pearson_correlation.Correlation_Values(max_value_of_correlation.value_of_categories_original,
                                                   max_value_of_correlation.value_random_of_categories,
                                                   max_value_of_correlation.value_of_corr)
                historial_of_value_correlation_for_end.append(objectTempEnd)

            max_value_of_correlationEnd = pearson_correlation.search_max_value_of_correlation(historial_of_value_correlation_for_end)
            objectResultEnd = pearson_correlation.Format_Result_Cat_Stadistical(int(position_column),
                                                            historial_of_value_correlation_for_end[
                                                                0].value_of_categories_original,
                                                            max_value_of_correlationEnd.value_random_of_categories,
                                                            max_value_of_correlationEnd.value_of_corr)
            resumen_of_result.append(objectResultEnd)
        information_for_text_area = []
        information_for_text_area.append("------------------------------------------")
        for resultss in resumen_of_result:
            information_for_text_area.append(
                "La columna categorica es la numero: " + str(resultss.number_of_column_categorical))

            information_for_text_area.append(
                "Valores originales de las categorias: " + str(resultss.value_categoria_inicial))

            information_for_text_area.append(
                "Valores para las categorias propuestos: " + str(resultss.value_categoria_final))

            information_for_text_area.append("El valor de correlación es: " + str(resultss.valor_correlation_max))
            columnDataset_Random = pearson_correlation.change_Categories_to_Categories_Random(
                dataset[:, resultss.number_of_column_categorical], resultss.value_categoria_inicial,
                resultss.value_categoria_final)

            dataset = pearson_correlation.add_column_random_to_dataset(dataset, columnDataset_Random, resultss.number_of_column_categorical)

            information_for_text_area.append("------------------------------------------")
        manage_database.insert_dataset(dataset)
        manage_database.update_data_type_column_categorical_to_numerics()

        correlation_matriz = pearson_correlation.pearson_correlations(dataset.T, 0)

        name = ''.join(random.choice('0123456789ABCDEF') for i in range(16))
        np.savetxt(str(str(name) + ".correlacion"), correlation_matriz, delimiter='\t', fmt="%s")
        np.savetxt(str(str(name) + ".info"), information_for_text_area, delimiter='\t', fmt="%s")

        return correlation_matriz, information_for_text_area



if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    Form = QtGui.QWidget()
    ui = Ui_FormStadistical_Categorical_Encode()
    ui.setupUi(Form)
    Form.show()
    sys.exit(app.exec_())

