# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'pca_view.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui
import manage_database
import pca
from PyQt4.QtGui import *
import math
import complementary_functions as cf
import manage_file as mf

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_FormPCA(object):
    def __init__(self):
        self.dataset = manage_database.obtain_dataset()
        self.flag_newdata = False

    def setupUi(self, FormPCA):
        FormPCA.setObjectName(_fromUtf8("FormPCA"))
        FormPCA.resize(762, 352)
        self.frame = QtGui.QFrame(FormPCA)
        self.frame.setGeometry(QtCore.QRect(10, 20, 241, 264))
        self.frame.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtGui.QFrame.Raised)
        self.frame.setObjectName(_fromUtf8("frame"))
        self.label = QtGui.QLabel(self.frame)
        self.label.setGeometry(QtCore.QRect(20, 10, 151, 17))
        self.label.setObjectName(_fromUtf8("label"))
        self.radioButton = QtGui.QRadioButton(self.frame)
        self.radioButton.setGeometry(QtCore.QRect(10, 45, 221, 22))
        self.radioButton.setObjectName(_fromUtf8("radioButton"))
        self.radioButton_2 = QtGui.QRadioButton(self.frame)
        self.radioButton_2.setGeometry(QtCore.QRect(10, 75, 117, 22))
        self.radioButton_2.setObjectName(_fromUtf8("radioButton_2"))
        self.radioButton_3 = QtGui.QRadioButton(self.frame)
        self.radioButton_3.setGeometry(QtCore.QRect(10, 110, 117, 20))
        self.radioButton_3.setText(_fromUtf8(""))
        self.radioButton_3.setObjectName(_fromUtf8("radioButton_3"))
        self.pushButton = QtGui.QPushButton(self.frame)
        self.pushButton.setGeometry(QtCore.QRect(140, 170, 81, 27))
        self.pushButton.setObjectName(_fromUtf8("pushButton"))
        self.pushButton.clicked.connect(self.execute_pca)
        self.lineEdit = QtGui.QLineEdit(self.frame)
        self.lineEdit.setGeometry(QtCore.QRect(36, 107, 87, 24))
        self.lineEdit.setObjectName(_fromUtf8("lineEdit"))
        self.pushButton_4 = QtGui.QPushButton(self.frame)
        self.pushButton_4.setGeometry(QtCore.QRect(10, 170, 99, 27))
        self.pushButton_4.setObjectName(_fromUtf8("pushButton_4"))
        self.pushButton_4.clicked.connect(self.saveFile)
        self.label_2 = QtGui.QLabel(self.frame)
        self.label_2.setGeometry(QtCore.QRect(5, 230, 241, 17))
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.tabWidget = QtGui.QTabWidget(FormPCA)
        self.tabWidget.setGeometry(QtCore.QRect(260, 10, 491, 264))
        self.tabWidget.setObjectName(_fromUtf8("tabWidget"))
        self.tab = QtGui.QWidget()
        self.tab.setObjectName(_fromUtf8("tab"))
        self.tableWidget_2 = QtGui.QTableWidget(self.tab)
        self.tableWidget_2.setGeometry(QtCore.QRect(0, 0, 487, 231))
        self.tableWidget_2.setObjectName(_fromUtf8("tableWidget_2"))
        self.tableWidget_2.setColumnCount(0)
        self.tableWidget_2.setRowCount(0)
        self.tabWidget.addTab(self.tab, _fromUtf8(""))
        self.tab_2 = QtGui.QWidget()
        self.tab_2.setObjectName(_fromUtf8("tab_2"))
        self.tableWidget = QtGui.QTableWidget(self.tab_2)
        self.tableWidget.setGeometry(QtCore.QRect(0, 0, 487, 231))
        self.tableWidget.setObjectName(_fromUtf8("tableWidget"))
        self.tableWidget.setColumnCount(0)
        self.tableWidget.setRowCount(0)
        self.tabWidget.addTab(self.tab_2, _fromUtf8(""))
        self.frame_2 = QtGui.QFrame(FormPCA)
        self.frame_2.setGeometry(QtCore.QRect(10, 290, 741, 51))
        self.frame_2.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame_2.setFrameShadow(QtGui.QFrame.Raised)
        self.frame_2.setObjectName(_fromUtf8("frame_2"))
        #self.pushButton_2 = QtGui.QPushButton(self.frame_2)
        #self.pushButton_2.setGeometry(QtCore.QRect(620, 10, 99, 27))
        #self.pushButton_2.setObjectName(_fromUtf8("pushButton_2"))
        #self.pushButton_2.clicked.connect(self.accept)
        self.pushButton_3 = QtGui.QPushButton(self.frame_2)
        self.pushButton_3.setGeometry(QtCore.QRect(30, 10, 99, 27))
        self.pushButton_3.setObjectName(_fromUtf8("pushButton_3"))
        self.pushButton_3.clicked.connect(FormPCA.close)

        self.retranslateUi(FormPCA)
        self.tabWidget.setCurrentIndex(1)
        QtCore.QMetaObject.connectSlotsByName(FormPCA)

    def retranslateUi(self, FormPCA):
        FormPCA.setWindowTitle(_translate("FormPCA", "PCA", None))
        self.label.setText(_translate("FormPCA", "Components Number", None))
        self.radioButton.setText(_translate("FormPCA", "min( n_samples,n_features)", None))
        self.radioButton_2.setText(_translate("FormPCA", "Estimates", None))
        self.pushButton.setText(_translate("FormPCA", "Execute", None))
        self.pushButton_4.setText(_translate("FormPCA", "Save", None))
        self.label_2.setText(_translate("FormPCA", "Number of Components Used: ?", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab), _translate("FormPCA", "Covariance", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_2), _translate("FormPCA", "Precision", None))
        #self.pushButton_2.setText(_translate("FormPCA", "Accept", None))
        self.pushButton_3.setText(_translate("FormPCA", "Close", None))


    def execute_pca(self):
        flag = True
        if self.radioButton.isChecked():
            nro1 = self.dataset.shape[0]
            nro2 = self.dataset.shape[1]
            if nro1<=nro2:
                number_component = nro1
            else:
                number_component = nro2
        elif self.radioButton_2.isChecked():
            number_component = 'mle'
        elif self.radioButton_3.isChecked():
            number_component = self.lineEdit.text()
            if number_component =="":
                cf.msg("Enter the components number")
                flag = False
            elif int(number_component)>self.dataset.shape[1]:
                cf.msg("The component number is greater than the existing one")
                flag = False
            else:
                number_component = int(number_component)

        else:
            cf.msg("Select the method for calculate the components number")
            flag = False

        if flag:
            try:
                self.newdata, covariance,precision,mean, explained_variance_, number_component_real =  pca.pca(self.dataset, number_component)
                self.flag_newdata = True
                print(explained_variance_)
                name_column = manage_database.obtain_name_column(0).tolist()
                self.print_in_table(self.tableWidget_2,covariance, name_column)
                self.print_in_table(self.tableWidget, precision, name_column)
                self.label_2.setText(_translate("FormPCA", "Number of Components Used: {}".format(number_component_real), None))
            except Exception:
                cf.msg("There are non-numerical values ")



    def print_in_table(self, name_component, dataset, name_column):
        name_component.setColumnCount(dataset.shape[1])  # rows and columns of table
        name_component.setRowCount(dataset.shape[0])
        name_component.setHorizontalHeaderLabels(name_column)
        name_component.setVerticalHeaderLabels(name_column)
        for row in range(0, dataset.shape[0]):  # add items from array to QTableWidget
            for column in range(0, dataset.shape[1]):
                name_component.setItem(row, column, QTableWidgetItem(str(dataset[row][column])))

    def saveFile(self):
        if self.flag_newdata:
            dir_save = QFileDialog().getSaveFileName()
            mf.savefile(dir_save, self.newdata)
        else:
            cf.msg("Execute PCA")

if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    FormPCA = QtGui.QWidget()
    ui = Ui_FormPCA()
    ui.setupUi(FormPCA)
    FormPCA.show()
    sys.exit(app.exec_())

