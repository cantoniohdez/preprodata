# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'define_structure.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui
from PyQt4.QtGui import *
import numpy as np
import manage_database as md
import manage_database
try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_FormDefineStructure(object):
    metadatos = []
    dataset = np.array([])
    Form = object
    def __init__(self):
        self.dataset = manage_database.obtain_structure()
        #print("Entrorrrrr")
        #print(self.dataset)


    def setupUi(self, Form):
        self.Form = Form
        self.Form.setObjectName(_fromUtf8("Form"))
        self.Form.resize(651, 453)
        self.frame = QtGui.QFrame(Form)
        self.frame.setGeometry(QtCore.QRect(10, 100, 631, 281))
        self.frame.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtGui.QFrame.Raised)
        self.frame.setObjectName(_fromUtf8("frame"))
        self.tableWidget = QtGui.QTableWidget(self.frame)
        self.tableWidget.setGeometry(QtCore.QRect(0, 0, 631, 281))
        self.tableWidget.setObjectName(_fromUtf8("tableWidget"))
        self.tableWidget.setColumnCount(4)
        self.tableWidget.setHorizontalHeaderLabels(["Field_name","Field_type","Length","Len_Dec"])
        self.tableWidget.setRowCount(0)
        self.frame_2 = QtGui.QFrame(Form)
        self.frame_2.setGeometry(QtCore.QRect(10, 390, 631, 51))
        self.frame_2.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame_2.setFrameShadow(QtGui.QFrame.Raised)
        self.frame_2.setObjectName(_fromUtf8("frame_2"))
        self.pushButton = QtGui.QPushButton(self.frame_2)
        self.pushButton.setGeometry(QtCore.QRect(490, 10, 99, 27))
        self.pushButton.setObjectName(_fromUtf8("pushButton"))
        self.pushButton.clicked.connect(self.accept)
        self.pushButton_2 = QtGui.QPushButton(self.frame_2)
        self.pushButton_2.setGeometry(QtCore.QRect(30, 10, 99, 27))
        self.pushButton_2.setObjectName(_fromUtf8("pushButton_2"))
        self.pushButton_2.clicked.connect(Form.close)
        self.frame_3 = QtGui.QFrame(Form)
        self.frame_3.setGeometry(QtCore.QRect(10, 10, 631, 80))
        self.frame_3.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame_3.setFrameShadow(QtGui.QFrame.Raised)
        self.frame_3.setObjectName(_fromUtf8("frame_3"))
        self.lineEdit = QtGui.QLineEdit(self.frame_3)
        self.lineEdit.setGeometry(QtCore.QRect(10, 40, 151, 27))
        self.lineEdit.setObjectName(_fromUtf8("lineEdit"))
        self.comboBox = QtGui.QComboBox(self.frame_3)
        self.comboBox.setGeometry(QtCore.QRect(180, 40, 111, 27))
        self.comboBox.setObjectName(_fromUtf8("comboBox"))
        self.comboBox.addItem(_fromUtf8(""))
        self.comboBox.addItem(_fromUtf8(""))
        self.comboBox.addItem(_fromUtf8(""))
        self.comboBox.addItem(_fromUtf8(""))
        self.lineEdit_2 = QtGui.QLineEdit(self.frame_3)
        self.lineEdit_2.setGeometry(QtCore.QRect(300, 40, 51, 27))
        self.lineEdit_2.setObjectName(_fromUtf8("lineEdit_2"))
        self.lineEdit_3 = QtGui.QLineEdit(self.frame_3)
        self.lineEdit_3.setGeometry(QtCore.QRect(370, 40, 51, 27))
        self.lineEdit_3.setObjectName(_fromUtf8("lineEdit_3"))
        self.label = QtGui.QLabel(self.frame_3)
        self.label.setGeometry(QtCore.QRect(10, 20, 151, 17))
        self.label.setObjectName(_fromUtf8("label"))
        self.label_2 = QtGui.QLabel(self.frame_3)
        self.label_2.setGeometry(QtCore.QRect(180, 20, 91, 17))
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.label_3 = QtGui.QLabel(self.frame_3)
        self.label_3.setGeometry(QtCore.QRect(300, 20, 61, 20))
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.label_4 = QtGui.QLabel(self.frame_3)
        self.label_4.setGeometry(QtCore.QRect(370, 20, 71, 17))
        self.label_4.setObjectName(_fromUtf8("label_4"))
        self.pushButton_3 = QtGui.QPushButton(self.frame_3)
        self.pushButton_3.setGeometry(QtCore.QRect(450, 40, 71, 27))
        self.pushButton_3.setObjectName(_fromUtf8("pushButton_3"))
        self.pushButton_3.clicked.connect(self.add_structure)
        self.pushButton_4 = QtGui.QPushButton(self.frame_3)
        self.pushButton_4.setGeometry(QtCore.QRect(530, 40, 71, 27))
        self.pushButton_4.setObjectName(_fromUtf8("pushButton_4"))
        self.pushButton_4.clicked.connect(self.delete_structure)

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

        regexp = QtCore.QRegExp('^([01]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])$')
        validator = QtGui.QRegExpValidator(regexp)
        self.lineEdit_2.setValidator(validator)
        self.lineEdit_3.setValidator(validator)


        if self.dataset.shape[0] != 0:
            self.tableWidget.setColumnCount(self.dataset.shape[1]-1)  # rows and columns of table
            self.tableWidget.setRowCount(self.dataset.shape[0])
            self.tableWidget.setItem(1, 1, QTableWidgetItem(self.dataset[0][0]))
            #print(self.dataset[0][1])
            for row in range(0, self.dataset.shape[0]):  # add items from array to QTableWidget
                for column in range(1, self.dataset.shape[1]):
                    self.tableWidget.setItem(row, column-1, QTableWidgetItem(str(self.dataset[row][column])))
        else:
            #print("La base de datos esta vacia")
            self.msg("The database is empty")

    def retranslateUi(self, Form):
        Form.setWindowTitle(_translate("Form", "Define Structure", None))
        self.pushButton.setText(_translate("Form", "Accept", None))
        self.pushButton_2.setText(_translate("Form", "Cancel", None))
        self.comboBox.setItemText(0, _translate("Form", "Select", None))
        self.comboBox.setItemText(1, _translate("Form", "String(C)", None))
        self.comboBox.setItemText(2, _translate("Form", "Numeric(N)", None))
        self.comboBox.setItemText(3, _translate("Form", "Date(D)", None))
        self.label.setText(_translate("Form", "Field name", None))
        self.label_2.setText(_translate("Form", "Field type", None))
        self.label_3.setText(_translate("Form", "Length", None))
        self.label_4.setText(_translate("Form", "Decimal", None))
        self.pushButton_3.setText(_translate("Form", "Add", None))
        self.pushButton_4.setText(_translate("Form", "Delete", None))

    def add_structure(self):
        field_name = self.lineEdit.text()
        length = self.lineEdit_2.text()
        len_dec = self.lineEdit_3.text()
        if field_name == "":
            self.msg("Enter Field Name")
        elif self.comboBox.currentText() == "Select":
            self.msg("Select Field Type")
        elif str(length)=="":
            self.msg("Enter Value Length")
        else:
            if self.comboBox.currentText() == "String(C)":
                field_type = "C"
            if self.comboBox.currentText() == "Numeric(N)":
                field_type = "N"
            if self.comboBox.currentText() == "Date(D)":
                field_type = "D"

            count_row = self.tableWidget.rowCount()
            self.tableWidget.setRowCount(count_row+1)
            self.tableWidget.setItem(count_row,0,QTableWidgetItem(str(field_name)))
            self.tableWidget.setItem(count_row, 1, QTableWidgetItem(str(field_type)))
            self.tableWidget.setItem(count_row, 2, QTableWidgetItem(str(length)))
            self.tableWidget.setItem(count_row, 3, QTableWidgetItem(str(len_dec)))
            self.lineEdit.setText("")
            self.lineEdit_2.setText("")
            self.lineEdit_3.setText("")
            self.comboBox.setCurrentIndex(0)


    def delete_structure(self):
        count_row = self.tableWidget.rowCount()
        count_row = self.tableWidget.rowCount()
        self.tableWidget.setRowCount(count_row - 1)

    def accept(self):
        #Esta linea evita que se adicionen mas campos a la estructura despues de crear una
        manage_database.deleteTableStructure()
        listComplete = []
        listNames = []
        for x in range(0,self.tableWidget.rowCount()):
            field_name = self.tableWidget.item(x, 0).text()
            field_type = str(self.tableWidget.item(x, 1).text()).upper()
            field_len = self.tableWidget.item(x, 2).text()
            field_len_dec = self.tableWidget.item(x, 3).text()
            objTemp = md.Metadatos(field_name,field_type,field_len,field_len_dec)
            listComplete.append(objTemp)
            listNames.append(field_name)

        if self.FindDuplicates(listNames)==False:
            manage_database.insert_metadatos(listComplete)
            manage_database.msg("Successful Operation")
            self.Form.close()
        else:
            self.msg("There is(are) duplicate(s) variable(s)")

    def FindDuplicates(self, in_list):
        #print(in_list)
        unique = set(in_list)
        for each in unique:
            count = in_list.count(each)
            if count > 1:
                return True
        return False

    def msg(self, text):
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Information)
        msg.setText(text)
        # msg.setInformativeText("This is additional information")
        msg.setWindowTitle("Information")
        msg.setDetailedText("The details are as follows:")
        msg.setStandardButtons(QMessageBox.Ok)
        msg.exec_()


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    Form = QtGui.QWidget()
    ui = Ui_FormDefineStructure()
    ui.setupUi(Form)
    Form.show()
    sys.exit(app.exec_())

