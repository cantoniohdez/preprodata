# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'chi_square_view.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_FormChiSquare(object):
    def setupUi(self, FormChiSquare):
        FormChiSquare.setObjectName(_fromUtf8("FormChiSquare"))
        FormChiSquare.resize(834, 599)
        self.frame = QtGui.QFrame(FormChiSquare)
        self.frame.setGeometry(QtCore.QRect(10, 10, 431, 281))
        self.frame.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtGui.QFrame.Raised)
        self.frame.setObjectName(_fromUtf8("frame"))
        self.listWidget = QtGui.QListWidget(self.frame)
        self.listWidget.setGeometry(QtCore.QRect(10, 30, 131, 111))
        self.listWidget.setObjectName(_fromUtf8("listWidget"))
        self.listWidget_2 = QtGui.QListWidget(self.frame)
        self.listWidget_2.setGeometry(QtCore.QRect(10, 150, 131, 121))
        self.listWidget_2.setObjectName(_fromUtf8("listWidget_2"))
        self.pushButton_3 = QtGui.QPushButton(self.frame)
        self.pushButton_3.setGeometry(QtCore.QRect(150, 60, 41, 27))
        self.pushButton_3.setObjectName(_fromUtf8("pushButton_3"))
        self.pushButton_4 = QtGui.QPushButton(self.frame)
        self.pushButton_4.setGeometry(QtCore.QRect(150, 180, 41, 27))
        self.pushButton_4.setObjectName(_fromUtf8("pushButton_4"))
        self.listWidget_3 = QtGui.QListWidget(self.frame)
        self.listWidget_3.setGeometry(QtCore.QRect(200, 30, 111, 111))
        self.listWidget_3.setObjectName(_fromUtf8("listWidget_3"))
        self.listWidget_4 = QtGui.QListWidget(self.frame)
        self.listWidget_4.setGeometry(QtCore.QRect(200, 150, 111, 121))
        self.listWidget_4.setObjectName(_fromUtf8("listWidget_4"))
        self.pushButton_5 = QtGui.QPushButton(self.frame)
        self.pushButton_5.setGeometry(QtCore.QRect(320, 60, 99, 27))
        self.pushButton_5.setObjectName(_fromUtf8("pushButton_5"))
        self.pushButton_6 = QtGui.QPushButton(self.frame)
        self.pushButton_6.setGeometry(QtCore.QRect(320, 100, 99, 27))
        self.pushButton_6.setObjectName(_fromUtf8("pushButton_6"))
        self.label_2 = QtGui.QLabel(self.frame)
        self.label_2.setGeometry(QtCore.QRect(14, 6, 251, 17))
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.frame_2 = QtGui.QFrame(FormChiSquare)
        self.frame_2.setGeometry(QtCore.QRect(10, 300, 431, 221))
        self.frame_2.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame_2.setFrameShadow(QtGui.QFrame.Raised)
        self.frame_2.setObjectName(_fromUtf8("frame_2"))
        self.listWidget_5 = QtGui.QListWidget(self.frame_2)
        self.listWidget_5.setGeometry(QtCore.QRect(10, 40, 161, 171))
        self.listWidget_5.setObjectName(_fromUtf8("listWidget_5"))
        self.label = QtGui.QLabel(self.frame_2)
        self.label.setGeometry(QtCore.QRect(10, 10, 221, 17))
        self.label.setObjectName(_fromUtf8("label"))
        self.pushButton_7 = QtGui.QPushButton(self.frame_2)
        self.pushButton_7.setGeometry(QtCore.QRect(290, 50, 99, 27))
        self.pushButton_7.setObjectName(_fromUtf8("pushButton_7"))
        self.pushButton_8 = QtGui.QPushButton(self.frame_2)
        self.pushButton_8.setGeometry(QtCore.QRect(290, 90, 99, 27))
        self.pushButton_8.setObjectName(_fromUtf8("pushButton_8"))
        self.frame_3 = QtGui.QFrame(FormChiSquare)
        self.frame_3.setGeometry(QtCore.QRect(10, 529, 821, 61))
        self.frame_3.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame_3.setFrameShadow(QtGui.QFrame.Raised)
        self.frame_3.setObjectName(_fromUtf8("frame_3"))
        self.pushButton = QtGui.QPushButton(self.frame_3)
        self.pushButton.setGeometry(QtCore.QRect(690, 20, 99, 27))
        self.pushButton.setObjectName(_fromUtf8("pushButton"))
        self.pushButton_2 = QtGui.QPushButton(self.frame_3)
        self.pushButton_2.setGeometry(QtCore.QRect(20, 20, 99, 27))
        self.pushButton_2.setObjectName(_fromUtf8("pushButton_2"))
        self.scrollArea = QtGui.QScrollArea(FormChiSquare)
        self.scrollArea.setGeometry(QtCore.QRect(450, 30, 381, 491))
        self.scrollArea.setWidgetResizable(True)
        self.scrollArea.setObjectName(_fromUtf8("scrollArea"))
        self.scrollAreaWidgetContents = QtGui.QWidget()
        self.scrollAreaWidgetContents.setGeometry(QtCore.QRect(0, 0, 379, 489))
        self.scrollAreaWidgetContents.setObjectName(_fromUtf8("scrollAreaWidgetContents"))
        self.scrollArea.setWidget(self.scrollAreaWidgetContents)
        self.label_3 = QtGui.QLabel(FormChiSquare)
        self.label_3.setGeometry(QtCore.QRect(450, 10, 191, 17))
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.pushButton_9 = QtGui.QPushButton(FormChiSquare)
        self.pushButton_9.setGeometry(QtCore.QRect(800, 0, 31, 27))
        self.pushButton_9.setObjectName(_fromUtf8("pushButton_9"))

        self.retranslateUi(FormChiSquare)
        QtCore.QMetaObject.connectSlotsByName(FormChiSquare)

    def retranslateUi(self, FormChiSquare):
        FormChiSquare.setWindowTitle(_translate("FormChiSquare", "Chi-square test ", None))
        self.pushButton_3.setText(_translate("FormChiSquare", ">>", None))
        self.pushButton_4.setText(_translate("FormChiSquare", ">>", None))
        self.pushButton_5.setText(_translate("FormChiSquare", "Test", None))
        self.pushButton_6.setText(_translate("FormChiSquare", "Clear", None))
        self.label_2.setText(_translate("FormChiSquare", "Selection of categorical variables", None))
        self.label.setText(_translate("FormChiSquare", "Eliminate categorical variables", None))
        self.pushButton_7.setText(_translate("FormChiSquare", "Delete", None))
        self.pushButton_8.setText(_translate("FormChiSquare", "Save", None))
        self.pushButton.setText(_translate("FormChiSquare", "Accept", None))
        self.pushButton_2.setText(_translate("FormChiSquare", "Cancel", None))
        self.label_3.setText(_translate("FormChiSquare", "Results    ", None))
        self.pushButton_9.setText(_translate("FormChiSquare", "?", None))


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    FormChiSquare = QtGui.QWidget()
    ui = Ui_FormChiSquare()
    ui.setupUi(FormChiSquare)
    FormChiSquare.show()
    sys.exit(app.exec_())

