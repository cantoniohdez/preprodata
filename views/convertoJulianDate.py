# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'convertoJulianDate.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName(_fromUtf8("Form"))
        Form.resize(400, 221)
        self.radioButton = QtGui.QRadioButton(Form)
        self.radioButton.setGeometry(QtCore.QRect(40, 30, 117, 22))
        self.radioButton.setObjectName(_fromUtf8("radioButton"))
        self.radioButton_2 = QtGui.QRadioButton(Form)
        self.radioButton_2.setGeometry(QtCore.QRect(40, 50, 117, 22))
        self.radioButton_2.setObjectName(_fromUtf8("radioButton_2"))
        self.radioButton_3 = QtGui.QRadioButton(Form)
        self.radioButton_3.setGeometry(QtCore.QRect(40, 90, 117, 22))
        self.radioButton_3.setObjectName(_fromUtf8("radioButton_3"))
        self.radioButton_4 = QtGui.QRadioButton(Form)
        self.radioButton_4.setGeometry(QtCore.QRect(40, 110, 117, 22))
        self.radioButton_4.setObjectName(_fromUtf8("radioButton_4"))
        self.radioButton_5 = QtGui.QRadioButton(Form)
        self.radioButton_5.setGeometry(QtCore.QRect(240, 30, 117, 22))
        self.radioButton_5.setObjectName(_fromUtf8("radioButton_5"))
        self.radioButton_6 = QtGui.QRadioButton(Form)
        self.radioButton_6.setGeometry(QtCore.QRect(240, 50, 117, 22))
        self.radioButton_6.setObjectName(_fromUtf8("radioButton_6"))
        self.radioButton_7 = QtGui.QRadioButton(Form)
        self.radioButton_7.setGeometry(QtCore.QRect(240, 90, 117, 22))
        self.radioButton_7.setObjectName(_fromUtf8("radioButton_7"))
        self.radioButton_8 = QtGui.QRadioButton(Form)
        self.radioButton_8.setGeometry(QtCore.QRect(240, 110, 117, 22))
        self.radioButton_8.setObjectName(_fromUtf8("radioButton_8"))
        self.pushButton = QtGui.QPushButton(Form)
        self.pushButton.setGeometry(QtCore.QRect(250, 170, 99, 27))
        self.pushButton.setObjectName(_fromUtf8("pushButton"))
        self.pushButton_2 = QtGui.QPushButton(Form)
        self.pushButton_2.setGeometry(QtCore.QRect(50, 170, 99, 27))
        self.pushButton_2.setObjectName(_fromUtf8("pushButton_2"))

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        Form.setWindowTitle(_translate("Form", "Convert to Julian Date", None))
        self.radioButton.setText(_translate("Form", "dd/mm/yyyy", None))
        self.radioButton_2.setText(_translate("Form", "dd/mm/yy", None))
        self.radioButton_3.setText(_translate("Form", "mm/dd/yyyy", None))
        self.radioButton_4.setText(_translate("Form", "mm/dd/yy", None))
        self.radioButton_5.setText(_translate("Form", "dd-mm-yyyy", None))
        self.radioButton_6.setText(_translate("Form", "dd-mm-yy", None))
        self.radioButton_7.setText(_translate("Form", "mm-dd-yyyy", None))
        self.radioButton_8.setText(_translate("Form", "mm-dd-yy", None))
        self.pushButton.setText(_translate("Form", "Convert", None))
        self.pushButton_2.setText(_translate("Form", "Cancel", None))


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    Form = QtGui.QWidget()
    ui = Ui_Form()
    ui.setupUi(Form)
    Form.show()
    sys.exit(app.exec_())

