# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'define_structure.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName(_fromUtf8("Form"))
        Form.resize(651, 453)
        self.frame = QtGui.QFrame(Form)
        self.frame.setGeometry(QtCore.QRect(10, 100, 631, 281))
        self.frame.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtGui.QFrame.Raised)
        self.frame.setObjectName(_fromUtf8("frame"))
        self.tableView = QtGui.QTableView(self.frame)
        self.tableView.setGeometry(QtCore.QRect(0, 0, 631, 281))
        self.tableView.setObjectName(_fromUtf8("tableView"))
        self.frame_2 = QtGui.QFrame(Form)
        self.frame_2.setGeometry(QtCore.QRect(10, 390, 631, 51))
        self.frame_2.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame_2.setFrameShadow(QtGui.QFrame.Raised)
        self.frame_2.setObjectName(_fromUtf8("frame_2"))
        self.pushButton = QtGui.QPushButton(self.frame_2)
        self.pushButton.setGeometry(QtCore.QRect(490, 10, 99, 27))
        self.pushButton.setObjectName(_fromUtf8("pushButton"))
        self.pushButton_2 = QtGui.QPushButton(self.frame_2)
        self.pushButton_2.setGeometry(QtCore.QRect(30, 10, 99, 27))
        self.pushButton_2.setObjectName(_fromUtf8("pushButton_2"))
        self.frame_3 = QtGui.QFrame(Form)
        self.frame_3.setGeometry(QtCore.QRect(10, 10, 631, 80))
        self.frame_3.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame_3.setFrameShadow(QtGui.QFrame.Raised)
        self.frame_3.setObjectName(_fromUtf8("frame_3"))
        self.lineEdit = QtGui.QLineEdit(self.frame_3)
        self.lineEdit.setGeometry(QtCore.QRect(10, 40, 151, 27))
        self.lineEdit.setObjectName(_fromUtf8("lineEdit"))
        self.comboBox = QtGui.QComboBox(self.frame_3)
        self.comboBox.setGeometry(QtCore.QRect(180, 40, 111, 27))
        self.comboBox.setObjectName(_fromUtf8("comboBox"))
        self.comboBox.addItem(_fromUtf8(""))
        self.comboBox.addItem(_fromUtf8(""))
        self.comboBox.addItem(_fromUtf8(""))
        self.comboBox.addItem(_fromUtf8(""))
        self.lineEdit_2 = QtGui.QLineEdit(self.frame_3)
        self.lineEdit_2.setGeometry(QtCore.QRect(300, 40, 51, 27))
        self.lineEdit_2.setObjectName(_fromUtf8("lineEdit_2"))
        self.lineEdit_3 = QtGui.QLineEdit(self.frame_3)
        self.lineEdit_3.setGeometry(QtCore.QRect(370, 40, 51, 27))
        self.lineEdit_3.setObjectName(_fromUtf8("lineEdit_3"))
        self.label = QtGui.QLabel(self.frame_3)
        self.label.setGeometry(QtCore.QRect(10, 20, 151, 17))
        self.label.setObjectName(_fromUtf8("label"))
        self.label_2 = QtGui.QLabel(self.frame_3)
        self.label_2.setGeometry(QtCore.QRect(180, 20, 91, 17))
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.label_3 = QtGui.QLabel(self.frame_3)
        self.label_3.setGeometry(QtCore.QRect(300, 20, 61, 20))
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.label_4 = QtGui.QLabel(self.frame_3)
        self.label_4.setGeometry(QtCore.QRect(370, 20, 71, 17))
        self.label_4.setObjectName(_fromUtf8("label_4"))
        self.pushButton_3 = QtGui.QPushButton(self.frame_3)
        self.pushButton_3.setGeometry(QtCore.QRect(450, 40, 71, 27))
        self.pushButton_3.setObjectName(_fromUtf8("pushButton_3"))
        self.pushButton_4 = QtGui.QPushButton(self.frame_3)
        self.pushButton_4.setGeometry(QtCore.QRect(530, 40, 71, 27))
        self.pushButton_4.setObjectName(_fromUtf8("pushButton_4"))

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        Form.setWindowTitle(_translate("Form", "Define Structure", None))
        self.pushButton.setText(_translate("Form", "Aceptar", None))
        self.pushButton_2.setText(_translate("Form", "Cancelar", None))
        self.comboBox.setItemText(0, _translate("Form", "Seleccione", None))
        self.comboBox.setItemText(1, _translate("Form", "Cadena(C)", None))
        self.comboBox.setItemText(2, _translate("Form", "Numérico(N)", None))
        self.comboBox.setItemText(3, _translate("Form", "Fecha(F)", None))
        self.label.setText(_translate("Form", "Field name", None))
        self.label_2.setText(_translate("Form", "Field type", None))
        self.label_3.setText(_translate("Form", "Length", None))
        self.label_4.setText(_translate("Form", "Decimal", None))
        self.pushButton_3.setText(_translate("Form", "Adicionar", None))
        self.pushButton_4.setText(_translate("Form", "Eliminar", None))


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    Form = QtGui.QWidget()
    ui = Ui_Form()
    ui.setupUi(Form)
    Form.show()
    sys.exit(app.exec_())

