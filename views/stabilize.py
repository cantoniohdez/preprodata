# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'stabilize.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from random import random
import math
import numpy as np
from PyQt4 import QtCore, QtGui
from PREPRODATolddddddd import Ui_MainWindow

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_FormStabilize(object):

    def setupUi(self, Form, dataset):
        self.datasetTemp = dataset
        Form.setObjectName(_fromUtf8("Form"))
        Form.resize(395, 157)
        self.frame = QtGui.QFrame(Form)
        self.frame.setGeometry(QtCore.QRect(10, 10, 381, 80))
        self.frame.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtGui.QFrame.Raised)
        self.frame.setObjectName(_fromUtf8("frame"))
        self.label = QtGui.QLabel(self.frame)
        self.label.setGeometry(QtCore.QRect(130, 10, 121, 17))
        self.label.setObjectName(_fromUtf8("label"))
        self.lineEdit = QtGui.QLineEdit(self.frame)
        self.lineEdit.setGeometry(QtCore.QRect(110, 30, 141, 27))
        self.lineEdit.setObjectName(_fromUtf8("lineEdit"))
        self.pushButton_3 = QtGui.QPushButton(self.frame)
        self.pushButton_3.setGeometry(QtCore.QRect(260, 30, 31, 27))
        self.pushButton_3.setText(_fromUtf8(""))
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(_fromUtf8("../icon/mas.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.pushButton_3.setIcon(icon)
        self.pushButton_3.setObjectName(_fromUtf8("pushButton_3"))
        self.pushButton_4 = QtGui.QPushButton(self.frame)
        self.pushButton_4.setGeometry(QtCore.QRect(70, 30, 31, 27))
        self.pushButton_4.setText(_fromUtf8(""))
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap(_fromUtf8("../icon/menos.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.pushButton_4.setIcon(icon1)
        self.pushButton_4.setObjectName(_fromUtf8("pushButton_4"))
        self.frame_2 = QtGui.QFrame(Form)
        self.frame_2.setGeometry(QtCore.QRect(10, 100, 381, 51))
        self.frame_2.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame_2.setFrameShadow(QtGui.QFrame.Raised)
        self.frame_2.setObjectName(_fromUtf8("frame_2"))
        self.pushButton = QtGui.QPushButton(self.frame_2)
        self.pushButton.setGeometry(QtCore.QRect(270, 10, 99, 27))
        self.pushButton.setObjectName(_fromUtf8("pushButton"))
        self.pushButton_2 = QtGui.QPushButton(self.frame_2)
        self.pushButton_2.setGeometry(QtCore.QRect(20, 10, 99, 27))
        self.pushButton_2.setObjectName(_fromUtf8("pushButton_2"))

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        Form.setWindowTitle(_translate("Form", "Stabilize", None))
        self.label.setText(_translate("Form", "Decimal Level", None))
        self.lineEdit.setText(_translate("Form", "0.0000001", None))
        self.pushButton.setText(_translate("Form", "Stabilize", None))
        self.pushButton_2.setText(_translate("Form", "Cancel", None))

    def stabilize(self):
        ##print(dataset.shape[0])
        mydecimal = float(self.lineEdit.text())
        mynewdataset = []
        for x in range(0,9):
            mylistTemp = []
            for y in range(0,1000):
                #Valor Aleatorio para estabilizar
                myrandom = random()
                #El valor actual que deseo estabilizar
                #print(self.datasetTemp.T.tolist()[x][y])
                myvalue = self.datasetTemp.T.tolist()[x][y]

                #Valor decimal que define el grado decimal a partir del cual voy hacer la estabilizacion
                #mydecimal = math.pow(10, -nivel)

                #Formula para el calculo del valor estabilizado
                if myvalue==0:
                    mynewvalue = (1+ mydecimal*myrandom)
                else:
                    mynewvalue = myvalue * (1+ mydecimal*myrandom)

                #Almaceno los datos de una columna en una lista temporal en cada caso
                mylistTemp.append(mynewvalue)

            #Agrego la lista temporal de una columna a la que va a contener todos los datos.
            mynewdataset.append(mylistTemp)
        self.datasetTemp = np.array(mynewdataset)
        #print(self.datasetTemp)
        xx = Ui_MainWindow(self.datasetTemp)
        xx.add_dataset_table()

if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    Form = QtGui.QWidget()
    ui = Ui_FormStabilize()
    ui.setupUi(Form)
    Form.show()
    sys.exit(app.exec_())

