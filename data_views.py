# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'data_views.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!


from PyQt4 import QtCore, QtGui
from PyQt4.QtGui import *
import numpy as np
from PyQt4 import QtGui
import matplotlib.pyplot as plt

import manage_database
import manage_file
import define_structure
import statistics_numeric
import statistics_string

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_FormDataViews(object):
    dataset = np.array([])

    def __init__(self):
        self.dataset = manage_database.obtain_dataset()


    def setupUi(self, Form):

        Form.setObjectName(_fromUtf8("Form"))
        Form.resize(784, 552)
        self.frame = QtGui.QFrame(Form)
        self.frame.setGeometry(QtCore.QRect(0, 0, 781, 31))
        self.frame.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtGui.QFrame.Raised)
        self.frame.setObjectName(_fromUtf8("frame"))
        self.pushButton = QtGui.QPushButton(self.frame)
        self.pushButton.setGeometry(QtCore.QRect(0, 0, 71, 27))
        self.pushButton.setEnabled(False)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(_fromUtf8("icon/Custom-Icon-Design-Pretty-Office-9-Open-file.ico")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.pushButton.setIcon(icon)
        self.pushButton.setObjectName(_fromUtf8("pushButton"))
        self.pushButton.clicked.connect(self.getfiles)
        self.pushButton_2 = QtGui.QPushButton(self.frame)
        self.pushButton_2.setGeometry(QtCore.QRect(70, 0, 71, 27))
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap(_fromUtf8("icon/diskette_save_download_technology_flat_icon-512.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.pushButton_2.setIcon(icon1)
        self.pushButton_2.setObjectName(_fromUtf8("pushButton_2"))
        self.pushButton_2.clicked.connect(self.savefile)
        self.pushButton_3 = QtGui.QPushButton(self.frame)
        self.pushButton_3.setGeometry(QtCore.QRect(240, 0, 71, 27))
        icon2 = QtGui.QIcon()
        icon2.addPixmap(QtGui.QPixmap(_fromUtf8("icon/close_windows.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.pushButton_3.setIcon(icon2)
        self.pushButton_3.setObjectName(_fromUtf8("pushButton_3"))
        self.pushButton_3.clicked.connect(Form.close)
        self.pushButton_4 = QtGui.QPushButton(self.frame)
        self.pushButton_4.setGeometry(QtCore.QRect(140, 0, 99, 27))
        self.pushButton_4.setIcon(icon1)
        self.pushButton_4.setObjectName(_fromUtf8("pushButton_4"))
        self.pushButton_4.clicked.connect(self.showDefineStructure)
        self.pushButton_5 = QtGui.QPushButton(self.frame)
        self.pushButton_5.setGeometry(QtCore.QRect(315, 0, 99, 27))
        self.pushButton_5.setObjectName(_fromUtf8("pushButton_4"))
        self.pushButton_5.clicked.connect(self.showStatistics)
        self.pushButton_6 = QtGui.QPushButton(self.frame)
        self.pushButton_6.setGeometry(QtCore.QRect(415, 0, 99, 27))
        self.pushButton_6.setObjectName(_fromUtf8("pushButton_4"))
        self.pushButton_6.clicked.connect(self.showTimeSeries)
        self.frame_2 = QtGui.QFrame(Form)
        self.frame_2.setGeometry(QtCore.QRect(0, 30, 781, 521))
        self.frame_2.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame_2.setFrameShadow(QtGui.QFrame.Raised)
        self.frame_2.setObjectName(_fromUtf8("frame_2"))
        self.tableWidget = QtGui.QTableWidget(self.frame_2)
        self.tableWidget.setGeometry(QtCore.QRect(0, 0, 781, 521))
        self.tableWidget.setObjectName(_fromUtf8("tableWidget"))
        self.tableWidget.setColumnCount(0)
        self.tableWidget.setRowCount(0)

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

        if self.dataset.shape[0] != 0:
            name_column = manage_database.obtain_name_column(0).tolist()
            if len(name_column) == self.dataset.shape[1]:

                self.tableWidget.setColumnCount(self.dataset.shape[1])  # rows and columns of table
                self.tableWidget.setRowCount(self.dataset.shape[0])
                #self.tableWidget.setItem(1, 1, QTableWidgetItem(self.dataset[0][0]))
                self.tableWidget.setHorizontalHeaderLabels(name_column)
                for row in range(0, self.dataset.shape[0]):  # add items from array to QTableWidget
                    for column in range(0, self.dataset.shape[1]):
                        self.tableWidget.setItem(row, column, QTableWidgetItem(str(self.dataset[row][column])))
                manage_database.insert_dataset(self.dataset)
            else:
                self.msg("The dataset and the defined structure are not equal")
        else:
            #print("La base de datos esta vacia")
            self.msg("The database is empty")

    def retranslateUi(self, Form):
        Form.setWindowTitle(_translate("Form", "Form", None))
        self.pushButton.setText(_translate("Form", "Open", None))
        self.pushButton_2.setText(_translate("Form", "Save", None))
        self.pushButton_3.setText(_translate("Form", "Close", None))
        self.pushButton_4.setText(_translate("Form", "Structure", None))
        self.pushButton_5.setText(_translate("Form", "Statistics", None))
        self.pushButton_6.setText(_translate("Form", "Time series", None))

    def getfiles(self):
        dlg = QFileDialog()
        dlg.setFileMode(QFileDialog.AnyFile)
        dlg.setFilter("Tab delimited text file (*.txt);;Comma delimited text file (*.csv)")
        if dlg.exec_():
            filenames = dlg.selectedFiles()
            #print("Hiiii")
            #print(str(filenames))
            if (str(filenames).split("."))[-1]=="csv']":
                print("Entro a CSV")
                #self.dataset = np.loadtxt(filenames[0],dtype=None)
                #print("Base de datos en la lectura")
                self.dataset = manage_file.loadCSV(filenames[0])
            else:
                print("Entro a txt")
                self.dataset = np.genfromtxt(filenames[0], dtype=None, delimiter = '\t')
            try:
                self.tableWidget.setColumnCount(self.dataset.shape[1])  # rows and columns of table
                self.tableWidget.setRowCount(self.dataset.shape[0])

                #self.tableWidget.setItem(1, 1, QTableWidgetItem(self.dataset[0][0]))
                name_column = manage_database.obtain_name_column(0).tolist()
                if len(name_column) == self.dataset.shape[1]:
                    self.tableWidget.setHorizontalHeaderLabels(name_column)
                    for row in range(0,self.dataset.shape[0]):  # add items from array to QTableWidget
                        for column in range(0,self.dataset.shape[1]):
                            if str(self.dataset[row][column])==str('?'):
                                self.dataset[row][column] = "NaN"
                                self.tableWidget.setItem(row, column, QTableWidgetItem(str(self.dataset[row][column])))
                            else:
                                self.tableWidget.setItem(row, column, QTableWidgetItem(str(self.dataset[row][column])))
                else:
                    self.msg("The dataset and the defined structure are not equal")

            except Exception:
                self.msg("Check the format of the dataset")


            manage_database.insert_dataset(self.dataset)



    def savefile(self):
        dir_save = QFileDialog().getSaveFileName()
        temp = dir_save.split(".")
        if str(temp[-1]) == "txt":
            np.savetxt(dir_save, self.dataset, delimiter='\t', fmt="%s")
        else:
            np.savetxt(dir_save+str(".txt"), self.dataset, delimiter='\t', fmt="%s")

    def showDefineStructure(self):
        self.pushButton.setEnabled(True)
        #manage_database.deleteTableStructure()
        self.Ui_FormDefineStructure = QtGui.QWidget()
        self.ui = define_structure.Ui_FormDefineStructure()
        self.ui.setupUi(self.Ui_FormDefineStructure)
        self.Ui_FormDefineStructure.show()

    def showStatistics(self):
        column_data = self.tableWidget.selectedItems()
        number_column = self.tableWidget.currentColumn()
        if column_data == []:
            self.msg("Please, select a data set")
        else:
            x = manage_database.obtain_structure()
            if(str(x[number_column,2])=="N"):
                #print("ENtro a N")
                self.Ui_FormStatistical = QtGui.QWidget()
                self.ui = statistics_numeric.Ui_FormStatistical(column_data)
                self.ui.setupUi(self.Ui_FormStatistical)
                self.Ui_FormStatistical.show()
            else:
                if (str(x[number_column, 2]) == "C"):
                    #print("Entro a C")
                    self.Ui_FormStatistical = QtGui.QWidget()
                    self.ui = statistics_string.Ui_FormStatistical(column_data)
                    self.ui.setupUi(self.Ui_FormStatistical)
                    self.Ui_FormStatistical.show()
                else:
                    self.msg("Please, the data is a date. Select other.")



    def showTimeSeries(self):
        column_data = self.tableWidget.selectedItems()
        column_dataList = []
        if column_data == []:
            self.msg("Please, select a data set")
        else:
            for x in column_data:
                value = x.text()
                try:
                    column_dataList.append(float(value))
                except Exception:
                    self.msg("There are not numeric values")

            y = np.array(column_dataList)
            x = np.array(list(range(y.shape[0])))


            #print(x)
            #print("--------")
            #print(y)
            plt.plot(x, y)
            plt.show()



    def msg(self, text):
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Information)
        msg.setText(text)
        # msg.setInformativeText("This is additional information")
        msg.setWindowTitle("Information")
        msg.setDetailedText("The details are as follows:")
        msg.setStandardButtons(QMessageBox.Ok)
        msg.exec_()

if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    Form = QtGui.QWidget()
    ui = Ui_FormDataViews()
    ui.setupUi(Form)
    Form.show()
    sys.exit(app.exec_())

