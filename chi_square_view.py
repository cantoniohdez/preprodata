# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'chi_square_view.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui
from PyQt4.QtGui import *
import numpy as np
from PyQt4 import QtGui

from builtins import list

import manage_database
import manage_file
import complementary_functions as cf
import numpy as np
try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_FormChiSquare(object):
    Form = object

    def __init__(self):
        self.dataset = manage_database.obtain_dataset()
        self.column_categorical = manage_database.obtain_categorical_columns()
        self.list_column_deleted = []

    def setupUi(self, FormChiSquare):
        self.Form = FormChiSquare
        FormChiSquare.setObjectName(_fromUtf8("FormChiSquare"))
        FormChiSquare.resize(834, 599)
        self.frame = QtGui.QFrame(FormChiSquare)
        self.frame.setGeometry(QtCore.QRect(10, 10, 431, 281))
        self.frame.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtGui.QFrame.Raised)
        self.frame.setObjectName(_fromUtf8("frame"))
        self.listWidget = QtGui.QListWidget(self.frame)
        self.listWidget.setGeometry(QtCore.QRect(10, 30, 131, 111))
        self.listWidget.setObjectName(_fromUtf8("listWidget"))
        self.listWidget_2 = QtGui.QListWidget(self.frame)
        self.listWidget_2.setGeometry(QtCore.QRect(10, 150, 131, 121))
        self.listWidget_2.setObjectName(_fromUtf8("listWidget_2"))
        self.pushButton_3 = QtGui.QPushButton(self.frame)
        self.pushButton_3.setGeometry(QtCore.QRect(150, 60, 41, 27))
        self.pushButton_3.setObjectName(_fromUtf8("pushButton_3"))
        self.pushButton_4 = QtGui.QPushButton(self.frame)
        self.pushButton_4.setGeometry(QtCore.QRect(150, 180, 41, 27))
        self.pushButton_4.setObjectName(_fromUtf8("pushButton_4"))
        self.listWidget_3 = QtGui.QListWidget(self.frame)
        self.listWidget_3.setGeometry(QtCore.QRect(200, 30, 111, 111))
        self.listWidget_3.setObjectName(_fromUtf8("listWidget_3"))
        self.pushButton_3.clicked.connect(self.move_variable1)
        self.listWidget_4 = QtGui.QListWidget(self.frame)
        self.listWidget_4.setGeometry(QtCore.QRect(200, 150, 111, 121))
        self.listWidget_4.setObjectName(_fromUtf8("listWidget_4"))
        self.pushButton_4.clicked.connect(self.move_variable2)
        self.pushButton_5 = QtGui.QPushButton(self.frame)
        self.pushButton_5.setGeometry(QtCore.QRect(320, 60, 99, 27))
        self.pushButton_5.setObjectName(_fromUtf8("pushButton_5"))
        self.pushButton_5.clicked.connect(self.execute_test)
        self.pushButton_6 = QtGui.QPushButton(self.frame)
        self.pushButton_6.setGeometry(QtCore.QRect(320, 100, 99, 27))
        self.pushButton_6.setObjectName(_fromUtf8("pushButton_6"))
        self.pushButton_6.clicked.connect(self.clear_listWidget)
        self.label_2 = QtGui.QLabel(self.frame)
        self.label_2.setGeometry(QtCore.QRect(14, 6, 251, 17))
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.frame_2 = QtGui.QFrame(FormChiSquare)
        self.frame_2.setGeometry(QtCore.QRect(10, 300, 431, 221))
        self.frame_2.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame_2.setFrameShadow(QtGui.QFrame.Raised)
        self.frame_2.setObjectName(_fromUtf8("frame_2"))
        self.listWidget_5 = QtGui.QListWidget(self.frame_2)
        self.listWidget_5.setGeometry(QtCore.QRect(10, 40, 161, 171))
        self.listWidget_5.setObjectName(_fromUtf8("listWidget_5"))
        self.label = QtGui.QLabel(self.frame_2)
        self.label.setGeometry(QtCore.QRect(10, 10, 221, 17))
        self.label.setObjectName(_fromUtf8("label"))
        self.pushButton_7 = QtGui.QPushButton(self.frame_2)
        self.pushButton_7.setGeometry(QtCore.QRect(290, 50, 99, 27))
        self.pushButton_7.setObjectName(_fromUtf8("pushButton_7"))
        self.pushButton_7.clicked.connect(self.delete_column)
        self.pushButton_8 = QtGui.QPushButton(self.frame_2)
        self.pushButton_8.setGeometry(QtCore.QRect(290, 90, 99, 27))
        self.pushButton_8.setObjectName(_fromUtf8("pushButton_8"))
        self.pushButton_8.clicked.connect(self.saveFile)
        self.frame_3 = QtGui.QFrame(FormChiSquare)
        self.frame_3.setGeometry(QtCore.QRect(10, 529, 821, 61))
        self.frame_3.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame_3.setFrameShadow(QtGui.QFrame.Raised)
        self.frame_3.setObjectName(_fromUtf8("frame_3"))
        self.pushButton = QtGui.QPushButton(self.frame_3)
        self.pushButton.setGeometry(QtCore.QRect(690, 20, 99, 27))
        self.pushButton.setObjectName(_fromUtf8("pushButton"))
        self.pushButton.clicked.connect(self.accept_change)
        self.pushButton_2 = QtGui.QPushButton(self.frame_3)
        self.pushButton_2.setGeometry(QtCore.QRect(20, 20, 99, 27))
        self.pushButton_2.setObjectName(_fromUtf8("pushButton_2"))
        self.pushButton_2.clicked.connect(FormChiSquare.close)

        self.tableWidget = QtGui.QTableWidget(FormChiSquare)
        self.tableWidget.setGeometry(QtCore.QRect(450, 30, 381, 491))
        self.tableWidget.setObjectName(_fromUtf8("tableWidget"))
        self.tableWidget.setColumnCount(1)
        self.tableWidget.setRowCount(0)
        vv = self.tableWidget.horizontalHeader()
        vv.setResizeMode(QHeaderView.ResizeToContents)
        vv.setStretchLastSection(True)

        self.label_3 = QtGui.QLabel(FormChiSquare)
        self.label_3.setGeometry(QtCore.QRect(450, 10, 191, 17))
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.pushButton_9 = QtGui.QPushButton(FormChiSquare)
        self.pushButton_9.setGeometry(QtCore.QRect(800, 0, 31, 27))
        self.pushButton_9.setObjectName(_fromUtf8("pushButton_9"))

        self.pushButton_3.setVisible(False)
        self.pushButton_4.setVisible(False)
        self.pushButton_6.setVisible(False)
        self.pushButton_3.setVisible(False)
        self.pushButton_3.setVisible(False)
        self.listWidget_3.setVisible(False)
        self.listWidget_4.setVisible(False)


        for x in self.column_categorical:
            item = QListWidgetItem(str(x))
            item2 = QListWidgetItem(str(x))
            item3 = QListWidgetItem(str(x))
            self.listWidget.addItem(item)
            self.listWidget_2.addItem(item2)
            self.listWidget_5.addItem(item3)

        self.retranslateUi(FormChiSquare)
        QtCore.QMetaObject.connectSlotsByName(FormChiSquare)

    def retranslateUi(self, FormChiSquare):
        FormChiSquare.setWindowTitle(_translate("FormChiSquare", "Chi-square test ", None))
        self.pushButton_3.setText(_translate("FormChiSquare", ">>", None))
        self.pushButton_4.setText(_translate("FormChiSquare", ">>", None))
        self.pushButton_5.setText(_translate("FormChiSquare", "Test", None))
        self.pushButton_6.setText(_translate("FormChiSquare", "Clear", None))
        self.label_2.setText(_translate("FormChiSquare", "Selection of categorical variables", None))
        self.label.setText(_translate("FormChiSquare", "Eliminate categorical variables", None))
        self.pushButton_7.setText(_translate("FormChiSquare", "Delete", None))
        self.pushButton_8.setText(_translate("FormChiSquare", "Save", None))
        self.pushButton.setText(_translate("FormChiSquare", "Accept", None))
        self.pushButton_2.setText(_translate("FormChiSquare", "Cancel", None))
        self.label_3.setText(_translate("FormChiSquare", "Results    ", None))
        self.pushButton_9.setText(_translate("FormChiSquare", "?", None))

    def move_variable1(self):

        try:
            value = self.listWidget.currentItem().text()
            self.listWidget_3.addItem(QListWidgetItem(value))
            self.pushButton_3.setEnabled(False)
        except Exception:
            cf.msg("Select a categorical variable")


    def move_variable2(self):
        try:
            value = self.listWidget_2.currentItem().text()
            self.listWidget_4.addItem(QListWidgetItem(value))
            self.pushButton_4.setEnabled(False)
        except Exception:
            cf.msg("Select a categorical variable")

    def clear_listWidget(self):
        self.listWidget_3.clear()
        self.listWidget_4.clear()
        self.pushButton_3.setEnabled(True)
        self.pushButton_4.setEnabled(True)

    def execute_test(self):
        #try:
        var1 = int(self.listWidget.currentItem().text())
        var2 = int(self.listWidget_2.currentItem().text())
        chi2, p, dof, ex = cf.chi_square_test(self.dataset, var1, var2)
        #print(chi2)
        self.tableWidget.setRowCount(3)
        self.tableWidget.setHorizontalHeaderLabels( ["Information"])
        self.tableWidget.setItem(0, 0, QTableWidgetItem("Chi-Square: {0}".format(chi2)))
        self.tableWidget.setItem(1, 0, QTableWidgetItem("Value of P: {0}".format(p)))
        self.tableWidget.setItem(2, 0, QTableWidgetItem("Degrees of freedom: {0}".format(dof)))


    def delete_column(self):
        column = self.listWidget_5.currentItem().text()
        position = self.listWidget_5.selectedIndexes()[0].row()
        self.list_column_deleted.append(int(column))
        self.column_categorical = np.delete(np.array(self.column_categorical), position)
        self.listWidget.clear()
        self.listWidget_2.clear()
        self.listWidget_5.clear()
        for x in self.column_categorical:
            item = QListWidgetItem(str(x))
            item2 = QListWidgetItem(str(x))
            item3 = QListWidgetItem(str(x))
            self.listWidget.addItem(item)
            self.listWidget_2.addItem(item2)
            self.listWidget_5.addItem(item3)

    def accept_change(self):

        #print("Lista de Columnas Eliminadas")
        list_sort = np.sort(np.array(self.list_column_deleted))[::-1]
        #print(list_sort)
        if len(self.list_column_deleted) == 0:
            cf.msg("There are no changes")
        else:
            for x in list_sort:
                #print("Value {0}".format(int(x)))
                manage_database.delete_element_structure(int(x))

            self.dataset = np.delete(self.dataset, list_sort.astype(int) , 1)

            #print(self.dataset)
            #print(self.dataset.shape)
            manage_database.deleteTableDataset()
            manage_database.insert_dataset(self.dataset)
            cf.msg("The modifications saved correctly")
        self.Form.close()



    def saveFile(self):

        #print("Lista de Columnas Eliminadas")
        list_sort = np.sort(np.array(self.list_column_deleted))[::-1]
        if len(self.list_column_deleted) == 0:
            cf.msg("There are no changes")
        else:

            dir_save = QFileDialog().getSaveFileName()
            temp = dir_save.split(".")
            if str(temp[-1]) == "txt":
                newDataset = np.delete(self.dataset, list_sort.astype(int), 1)
                np.savetxt(dir_save, newDataset, delimiter='\t', fmt="%s")
            else:
                newDataset = np.delete(self.dataset, list_sort.astype(int), 1)
                np.savetxt(dir_save + str(".txt"), newDataset, delimiter='\t', fmt="%s")



if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    FormChiSquare = QtGui.QWidget()
    ui = Ui_FormChiSquare()
    ui.setupUi(FormChiSquare)
    FormChiSquare.show()
    sys.exit(app.exec_())

