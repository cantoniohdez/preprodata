# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'missing_value_view.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui
from PyQt4.QtGui import *
import scipy as sc
import manage_database
import pandas as pd
import complementary_functions as cf
import manage_file as mf
import general_data_views

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)


class Ui_FormMissingValue(object):

    def __init__(self):
        self.dataset = manage_database.obtain_dataset()
        self.missing_value_total, self.newData = cf.count_missing_values(self.dataset)
        self.entropy_general = cf.calculate_entropy2(self.newData)
        self.entropy_delete = 0
        self.entropy_spline = 0

        if self.missing_value_total == 0:
            cf.msg("There are not missing values")

    def setupUi(self, Form):
        Form.setObjectName(_fromUtf8("Form"))
        Form.resize(652, 293)
        self.frame = QtGui.QFrame(Form)
        self.frame.setGeometry(QtCore.QRect(10, 80, 311, 141))
        self.frame.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtGui.QFrame.Raised)
        self.frame.setObjectName(_fromUtf8("frame"))
        self.label_3 = QtGui.QLabel(self.frame)
        self.label_3.setGeometry(QtCore.QRect(90, 10, 111, 17))
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.label_5 = QtGui.QLabel(self.frame)
        self.label_5.setGeometry(QtCore.QRect(20, 40, 221, 17))
        self.label_5.setObjectName(_fromUtf8("label_5"))
        self.pushButton = QtGui.QPushButton(self.frame)
        self.pushButton.setGeometry(QtCore.QRect(208, 100, 81, 27))
        self.pushButton.setObjectName(_fromUtf8("pushButton"))
        self.pushButton.clicked.connect(self.delete_tuples_with_missing_value)
        self.pushButton_2 = QtGui.QPushButton(self.frame)
        self.pushButton_2.setGeometry(QtCore.QRect(110, 100, 81, 27))
        self.pushButton_2.setObjectName(_fromUtf8("pushButton_2"))
        self.pushButton_2.setVisible(False)
        self.pushButton_2.clicked.connect(self.open_view_datasetDelete)
        self.pushButton_3 = QtGui.QPushButton(self.frame)
        self.pushButton_3.setGeometry(QtCore.QRect(20, 100, 71, 27))
        self.pushButton_3.setObjectName(_fromUtf8("pushButton_3"))
        self.pushButton_3.setVisible(False)
        self.pushButton_3.clicked.connect(self.saveFileTuplesDelete)
        self.frame_2 = QtGui.QFrame(Form)
        self.frame_2.setGeometry(QtCore.QRect(330, 80, 311, 141))
        self.frame_2.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame_2.setFrameShadow(QtGui.QFrame.Raised)
        self.frame_2.setObjectName(_fromUtf8("frame_2"))
        self.label_4 = QtGui.QLabel(self.frame_2)
        self.label_4.setGeometry(QtCore.QRect(70, 10, 161, 17))
        self.label_4.setObjectName(_fromUtf8("label_4"))
        self.label_8 = QtGui.QLabel(self.frame_2)
        self.label_8.setGeometry(QtCore.QRect(40, 40, 91, 17))
        self.label_8.setObjectName(_fromUtf8("label_8"))
        self.pushButton_4 = QtGui.QPushButton(self.frame_2)
        self.pushButton_4.setGeometry(QtCore.QRect(208, 100, 81, 27))
        self.pushButton_4.setObjectName(_fromUtf8("pushButton_4"))
        self.pushButton_4.clicked.connect(self.missing_value_using_natural_spline)
        self.pushButton_5 = QtGui.QPushButton(self.frame_2)
        self.pushButton_5.setGeometry(QtCore.QRect(108, 100, 81, 27))
        self.pushButton_5.setObjectName(_fromUtf8("pushButton_5"))
        self.pushButton_5.setVisible(False)
        self.pushButton_5.clicked.connect(self.open_view_datasetCompleteSpline)
        self.pushButton_6 = QtGui.QPushButton(self.frame_2)
        self.pushButton_6.setGeometry(QtCore.QRect(18, 100, 81, 27))
        self.pushButton_6.setObjectName(_fromUtf8("pushButton_6"))
        self.pushButton_6.setVisible(False)
        self.pushButton_6.clicked.connect(self.saveFileTuplesCompleteSpline)
        self.frame_3 = QtGui.QFrame(Form)
        self.frame_3.setGeometry(QtCore.QRect(10, 10, 631, 61))
        self.frame_3.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame_3.setFrameShadow(QtGui.QFrame.Raised)
        self.frame_3.setObjectName(_fromUtf8("frame_3"))
        self.label = QtGui.QLabel(self.frame_3)
        self.label.setGeometry(QtCore.QRect(20, 10, 221, 17))
        self.label.setObjectName(_fromUtf8("label"))
        self.label_2 = QtGui.QLabel(self.frame_3)
        self.label_2.setGeometry(QtCore.QRect(140, 30, 130, 17))
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.label_6 = QtGui.QLabel(self.frame_3)
        self.label_6.setGeometry(QtCore.QRect(287, 10, 281, 20))
        self.label_6.setObjectName(_fromUtf8("label_6"))
        self.label_7 = QtGui.QLabel(self.frame_3)
        self.label_7.setGeometry(QtCore.QRect(420, 30, 151, 17))
        self.label_7.setObjectName(_fromUtf8("label_7"))
        self.frame_4 = QtGui.QFrame(Form)
        self.frame_4.setGeometry(QtCore.QRect(10, 230, 631, 51))
        self.frame_4.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame_4.setFrameShadow(QtGui.QFrame.Raised)
        self.frame_4.setObjectName(_fromUtf8("frame_4"))
        self.pushButton_7 = QtGui.QPushButton(self.frame_4)
        self.pushButton_7.setGeometry(QtCore.QRect(510, 10, 99, 27))
        self.pushButton_7.setObjectName(_fromUtf8("pushButton_7"))
        self.pushButton_7.clicked.connect(self.accept_save_database)
        self.pushButton_8 = QtGui.QPushButton(self.frame_4)
        self.pushButton_8.setGeometry(QtCore.QRect(30, 10, 99, 27))
        self.pushButton_8.setObjectName(_fromUtf8("pushButton_8"))
        self.pushButton_8.clicked.connect(Form.close)

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        Form.setWindowTitle(_translate("Form", "Missing Values", None))
        self.label_3.setText(_translate("Form", "Delete tuples", None))
        self.label_5.setText(_translate("Form", "Entropy: ", None))
        self.pushButton.setText(_translate("Form", "Delete", None))
        self.pushButton_2.setText(_translate("Form", "View", None))
        self.pushButton_3.setText(_translate("Form", "Save", None))
        self.label_4.setText(_translate("Form", "Complete with spline", None))
        self.label_8.setText(_translate("Form", "Entropy:", None))
        self.pushButton_4.setText(_translate("Form", "Complete", None))
        self.pushButton_5.setText(_translate("Form", "View", None))
        self.pushButton_6.setText(_translate("Form", "Save", None))
        self.label.setText(_translate("Form", "Count of missing values(NaN): {0}".format(self.missing_value_total), None))
        self.label_2.setText(_translate("Form", "Entropy: {0}".format(round(self.entropy_general,5)), None))
        self.label_6.setText(_translate("Form", "Count of tuples with missing values:", None))
        self.label_7.setText(_translate("Form", "Count of tuples:", None))
        self.pushButton_7.setText(_translate("Form", "Accept", None))
        self.pushButton_8.setText(_translate("Form", "Close", None))

    def delete_tuples_with_missing_value(self):
        if self.missing_value_total ==0:
            cf.msg("There are not missing values")
        else:
            self.new_dataset_delete, self.entropy_delete = cf.delete_row_with_missing_value(self.dataset)
            self.label_5.setText(_translate("Form", "Entropy: {0}".format(round(self.entropy_delete,5)), None))
            self.pushButton_3.setVisible(True)
            self.pushButton_2.setVisible(True)

    def saveFileTuplesDelete(self):
        dir_save = QFileDialog().getSaveFileName()
        mf.savefile(dir_save, self.new_dataset_delete)

    def saveFileTuplesCompleteSpline(self):
        dir_save = QFileDialog().getSaveFileName()
        mf.savefile(dir_save, self.dataset_with_imputation)

    def missing_value_using_natural_spline(self):
        if self.missing_value_total ==0:
            cf.msg("There are not missing values")
        else:
            self.dataset_with_imputation = cf.imputation_value_spline(self.dataset)
            self.entropy_spline =cf.calculate_entropy2(self.dataset_with_imputation)
            self.label_8.setText(_translate("Form", "Entropy: {0}".format(round(self.entropy_spline, 5)), None))
            self.pushButton_6.setVisible(True)
            self.pushButton_5.setVisible(True)


    def open_view_datasetCompleteSpline(self):
        self.FormDataViewsGeneral = QtGui.QWidget()
        self.ui = general_data_views.Ui_FormDataViewsGeneral(self.dataset_with_imputation)
        self.ui.setupUi(self.FormDataViewsGeneral)
        self.FormDataViewsGeneral.show()

    def open_view_datasetDelete(self):
        self.FormDataViewsGeneral = QtGui.QWidget()
        self.ui = general_data_views.Ui_FormDataViewsGeneral(self.new_dataset_delete)
        self.ui.setupUi(self.FormDataViewsGeneral)
        self.FormDataViewsGeneral.show()

    def accept_save_database(self):
        if self.pushButton_6.isVisible() and self.pushButton_3.isVisible():
            if self.entropy_spline - self.entropy_delete <1:
                manage_database.insert_dataset(self.new_dataset_delete)
                cf.msg("Delete")
            else:
                manage_database.insert_dataset(self.dataset_with_imputation)
                cf.msg("Imputation with Spline")
        else:
            if self.pushButton_6.isVisible():
                manage_database.insert_dataset(self.dataset_with_imputation)
            elif self.pushButton_3.isVisible():
                manage_database.insert_dataset(self.new_dataset_delete)
            else:
                cf.msg("Execute a method")







if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    Form = QtGui.QWidget()
    ui = Ui_FormMissingValue()
    ui.setupUi(Form)
    Form.show()
    sys.exit(app.exec_())

