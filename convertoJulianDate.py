# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'convertoJulianDate.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!


from PyQt4 import QtCore, QtGui
from sklearn import preprocessing
import numpy as np
from PyQt4.QtGui import *
import manage_database
try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_FormJulian(object):
    dataset = np.array([])
    Form = object

    def setupUi(self, Form):
        self.Form = Form
        self.Form.setObjectName(_fromUtf8("Form"))
        self.Form.resize(400, 221)
        self.radioButton = QtGui.QRadioButton(Form)
        self.radioButton.setGeometry(QtCore.QRect(40, 30, 117, 22))
        self.radioButton.setObjectName(_fromUtf8("radioButton"))
        self.radioButton_2 = QtGui.QRadioButton(Form)
        self.radioButton_2.setGeometry(QtCore.QRect(40, 50, 117, 22))
        self.radioButton_2.setObjectName(_fromUtf8("radioButton_2"))
        self.radioButton_3 = QtGui.QRadioButton(Form)
        self.radioButton_3.setGeometry(QtCore.QRect(40, 90, 117, 22))
        self.radioButton_3.setObjectName(_fromUtf8("radioButton_3"))
        self.radioButton_4 = QtGui.QRadioButton(Form)
        self.radioButton_4.setGeometry(QtCore.QRect(40, 110, 117, 22))
        self.radioButton_4.setObjectName(_fromUtf8("radioButton_4"))
        self.radioButton_5 = QtGui.QRadioButton(Form)
        self.radioButton_5.setGeometry(QtCore.QRect(240, 30, 117, 22))
        self.radioButton_5.setObjectName(_fromUtf8("radioButton_5"))
        self.radioButton_6 = QtGui.QRadioButton(Form)
        self.radioButton_6.setGeometry(QtCore.QRect(240, 50, 117, 22))
        self.radioButton_6.setObjectName(_fromUtf8("radioButton_6"))
        self.radioButton_7 = QtGui.QRadioButton(Form)
        self.radioButton_7.setGeometry(QtCore.QRect(240, 90, 117, 22))
        self.radioButton_7.setObjectName(_fromUtf8("radioButton_7"))
        self.radioButton_8 = QtGui.QRadioButton(Form)
        self.radioButton_8.setGeometry(QtCore.QRect(240, 110, 117, 22))
        self.radioButton_8.setObjectName(_fromUtf8("radioButton_8"))
        self.pushButton = QtGui.QPushButton(Form)
        self.pushButton.setGeometry(QtCore.QRect(250, 170, 99, 27))
        self.pushButton.setObjectName(_fromUtf8("pushButton"))
        self.pushButton.clicked.connect(self.convert_to_julian)
        self.pushButton_2 = QtGui.QPushButton(Form)
        self.pushButton_2.setGeometry(QtCore.QRect(50, 170, 99, 27))
        self.pushButton_2.setObjectName(_fromUtf8("pushButton_2"))
        self.pushButton_2.clicked.connect(Form.close)

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        Form.setWindowTitle(_translate("Form", "Convert to Julian Date", None))
        self.radioButton.setText(_translate("Form", "dd/mm/yyyy", None))
        self.radioButton_2.setText(_translate("Form", "dd/mm/yy", None))
        self.radioButton_3.setText(_translate("Form", "mm/dd/yyyy", None))
        self.radioButton_4.setText(_translate("Form", "mm/dd/yy", None))
        self.radioButton_5.setText(_translate("Form", "dd-mm-yyyy", None))
        self.radioButton_6.setText(_translate("Form", "dd-mm-yy", None))
        self.radioButton_7.setText(_translate("Form", "mm-dd-yyyy", None))
        self.radioButton_8.setText(_translate("Form", "mm-dd-yy", None))
        self.pushButton.setText(_translate("Form", "Convert", None))
        self.pushButton_2.setText(_translate("Form", "Cancel", None))

    def convert_to_julian(self):
        self.dataset = manage_database.obtain_dataset()
        date_columns = manage_database.obtain_date_columns()
        #print("Lista de columnas de fechas")
        #print(date_columns)
        #print("Para ver si carga bien el dataset en Escalar")
        for y in date_columns:
            date = self.dataset[:, int(y)]
            #print("Columna de Fechas")
            #print(date)
            julian_date = []
            for x in range(0, len(date)):
                value_date = date[x].split("/")
                dia = float(value_date[0])
                mes = float(value_date[1])
                ano = float(value_date[2])
                #print("Dia")
                #print(dia)
                #print("Mes")
                #print(mes)
                #print("Anoo")
                #print(ano)
                a = ano / 100
                b = (2 - a) + (a / 4)
                #print("AAAA")
                #print(a)
                #print("BBB")
                #print(b)
                #value_julian = float((365.25 * (ano + 4716))) + float((30.6001 * (mes + 1))) + dia + b - 1524.5
                value_julian = (1461*(ano+4800+(mes-14)/12))/4+ (367*(mes-2-12*((mes-14)/12)))/12-(3*((ano+4900+(mes-14)/12)/100))/4 + dia -32075
                #value_julian = dia-32075+1461*(ano+4800+(mes-14)/12)/4+367*(mes-2-(mes-14)/12*12)/12-3*((ano+4900+(mes-14)/12)/100)/4
                #print(value_date)
                #print(value_julian)
                julian_date.append(str(value_julian))
            #print(self.dataset)
            list2 = np.delete(self.dataset, int(y), 1).T.tolist()
            #print(list2)
            #print(type(list2))
            #print(julian_date)
            #print(type(julian_date))
            list2.insert(int(y), julian_date)
            self.dataset = np.array(list2).T
        #print("Info del Dataset Final")
        #print(self.dataset)
        #print(self.dataset.shape)

        manage_database.insert_dataset(self.dataset)
        manage_database.update_data_type_column_date_to_numerics()
        manage_database.msg(self, "Successful Operation")
        self.Form.close()



if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    Form = QtGui.QWidget()
    ui = Ui_FormJulian()
    ui.setupUi(Form)
    Form.show()
    sys.exit(app.exec_())

