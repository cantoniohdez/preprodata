# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'statistical.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui
import numpy as np
import scipy as sc
from PyQt4 import QtGui
from PyQt4.QtGui import *
import matplotlib.pyplot as plt

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_FormStatistical(object):

    def __init__(self,data):
        self.data_column = []

        self.numberofvalues = 0
        self.numberofMissingValues = 0
        self.sum = 0
        self.sumsquare = 0

        for x in data:
            self.numberofvalues+=1
            value = x.text()
            try:
                if value == "NaN":
                    self.numberofMissingValues+=1
                else:
                    self.data_column.append(float(value))
                    self.sum+=float(value)
                    self.sumsquare+= float(value)*float(value)
            except Exception:
                self.msg("There are not numeric values")

        self.minimum = np.amin(self.data_column)
        self.maximum = np.amax(self.data_column)
        self.variance = np.var(self.data_column)
        self.standardDeviation = np.std(self.data_column)
        self.mean = np.mean(self.data_column)





    def setupUi(self, Form):
        Form.setObjectName(_fromUtf8("Form"))
        Form.resize(349, 388)
        self.frame = QtGui.QFrame(Form)
        self.frame.setGeometry(QtCore.QRect(10, 10, 331, 301))
        self.frame.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtGui.QFrame.Raised)
        self.frame.setObjectName(_fromUtf8("frame"))
        self.tableWidget = QtGui.QTableWidget(self.frame)
        self.tableWidget.setGeometry(QtCore.QRect(10, 10, 311, 281))
        self.tableWidget.setObjectName(_fromUtf8("tableWidget"))
        self.tableWidget.setColumnCount(1)
        self.tableWidget.setRowCount(8)
        item = QtGui.QTableWidgetItem()
        self.tableWidget.setVerticalHeaderItem(0, item)
        item = QtGui.QTableWidgetItem()
        self.tableWidget.setVerticalHeaderItem(1, item)
        item = QtGui.QTableWidgetItem()
        self.tableWidget.setVerticalHeaderItem(2, item)
        item = QtGui.QTableWidgetItem()
        self.tableWidget.setVerticalHeaderItem(3, item)
        item = QtGui.QTableWidgetItem()
        self.tableWidget.setVerticalHeaderItem(4, item)
        item = QtGui.QTableWidgetItem()
        self.tableWidget.setVerticalHeaderItem(5, item)
        item = QtGui.QTableWidgetItem()
        self.tableWidget.setVerticalHeaderItem(6, item)
        item = QtGui.QTableWidgetItem()
        self.tableWidget.setVerticalHeaderItem(7, item)
        item = QtGui.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(0, item)
        self.frame_2 = QtGui.QFrame(Form)
        self.frame_2.setGeometry(QtCore.QRect(10, 330, 331, 51))
        self.frame_2.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame_2.setFrameShadow(QtGui.QFrame.Raised)
        self.frame_2.setObjectName(_fromUtf8("frame_2"))
        self.pushButton = QtGui.QPushButton(self.frame_2)
        self.pushButton.setGeometry(QtCore.QRect(220, 10, 99, 27))
        self.pushButton.setObjectName(_fromUtf8("pushButton"))
        self.pushButton.clicked.connect(Form.close)

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

        self.tableWidget.setItem(1, -1, QTableWidgetItem(str(self.minimum)))
        self.tableWidget.setItem(1, 0, QTableWidgetItem(str(self.maximum)))
        self.tableWidget.setItem(1, 1, QTableWidgetItem(str(self.variance)))
        self.tableWidget.setItem(1, 2, QTableWidgetItem(str(self.standardDeviation)))
        self.tableWidget.setItem(1, 3, QTableWidgetItem(str(self.sum)))
        self.tableWidget.setItem(1, 4, QTableWidgetItem(str(self.sumsquare)))
        self.tableWidget.setItem(1, 5, QTableWidgetItem(str(self.numberofvalues)))
        self.tableWidget.setItem(1, 6, QTableWidgetItem(str(self.numberofMissingValues)))

    def retranslateUi(self, Form):
        Form.setWindowTitle(_translate("Form", "Statistics", None))
        item = self.tableWidget.verticalHeaderItem(0)
        item.setText(_translate("Form", "Minimum", None))
        item = self.tableWidget.verticalHeaderItem(1)
        item.setText(_translate("Form", "Maximum", None))
        item = self.tableWidget.verticalHeaderItem(2)
        item.setText(_translate("Form", "Variance", None))
        item = self.tableWidget.verticalHeaderItem(3)
        item.setText(_translate("Form", "Standard Deviation", None))
        item = self.tableWidget.verticalHeaderItem(4)
        item.setText(_translate("Form", "Sum", None))
        item = self.tableWidget.verticalHeaderItem(5)
        item.setText(_translate("Form", "Sum of Squares", None))
        item = self.tableWidget.verticalHeaderItem(6)
        item.setText(_translate("Form", "Number of Values", None))
        item = self.tableWidget.verticalHeaderItem(7)
        item.setText(_translate("Form", "Number of missing Values", None))
        item = self.tableWidget.horizontalHeaderItem(0)
        item.setText(_translate("Form", "Selection", None))
        self.pushButton.setText(_translate("Form", "Close", None))

    def msg(self, text):
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Information)
        msg.setText(text)
        # msg.setInformativeText("This is additional information")
        msg.setWindowTitle("Information")
        msg.setDetailedText("The details are as follows:")
        msg.setStandardButtons(QMessageBox.Ok)
        msg.exec_()

if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    Form = QtGui.QWidget()
    ui = Ui_FormStatistical()
    ui.setupUi(Form)
    Form.show()
    sys.exit(app.exec_())

