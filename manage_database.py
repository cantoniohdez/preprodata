import sqlite3
#from grp import struct_group

import numpy as np
from PyQt4.QtGui import *


def insert_dataset(dataset):
    #print("Base de Datos")
    conn=sqlite3.connect('PREPRODAT.db')
    print ("Database created and opened succesfully")
    c =conn.cursor()
    deleteTableDataset()
    #CREATE TABLE
    attributtesCREATETABLE = "("
    #print("Insertttttttt")
    #print(dataset.shape)
    for x in range(0,dataset.shape[1]):
        if x!=0:
            attributtesCREATETABLE = attributtesCREATETABLE+","+str("Column")+str(x)+" text"
        else:
            attributtesCREATETABLE = attributtesCREATETABLE+str("Column")+str(x)+ " text"
    attributtesCREATETABLE = attributtesCREATETABLE+str(")")
    #print("Atributos para la creacion de la Tabla")
    #print(attributtesCREATETABLE)
    queryCREATETABLE = '''CREATE TABLE IF NOT EXISTS datasetDB {0} '''.format(attributtesCREATETABLE)
    c.execute(queryCREATETABLE)
    #print("OK-->>CREATE TABLE")
    #INSERT DATA IN TABLE
    #print(dataset[0,0])
    for i in range(0,dataset.shape[0]):
        attributtesInsert = "("
        for j in range(0, dataset.shape[1]):
            if j != 0:
                attributtesInsert = attributtesInsert + ",'"+ str(dataset[i,j])+"'"
            else:
                attributtesInsert = attributtesInsert + "'"+str(dataset[i,j])+"'"
        attributtesInsert = attributtesInsert+str(")")
        #print(attributtesInsert)
        queryInsert = '''INSERT INTO datasetDB VALUES {0} '''.format(attributtesInsert)
        #print("Consulta que se realizo para Insertar")
        #print(queryInsert)
        c.execute(queryInsert)
        #print("OK-->>Insert")
    conn.commit()
    c.close()
    conn.close()

def insert_metadatos(metadatos):
    ##print("Metadatos")
    ##print(metadatos)
    ##print("Base de Datos")
    conn = sqlite3.connect('PREPRODAT.db')
    #print ("Database created and opened succesfully")
    c = conn.cursor()
    deleteTableDataset()
    # CREATE TABLE
    attributtesCREATETABLE = "( Number text, Field_name text, Field_type text, length text, len_dec text )"


    queryCREATETABLE = '''CREATE TABLE IF NOT EXISTS structureDB {0} '''.format(attributtesCREATETABLE)
    c.execute(queryCREATETABLE)
    ##print("OK-->>CREATE TABLE")
    # INSERT DATA IN TABLE
    for i in range(0, len(metadatos)):
        attributtesInsert = "('"+str(i)+"','"+str(metadatos[i].field_name)+"','"+str(metadatos[i].field_type)+"','"+str(metadatos[i].length)+"','"+str(metadatos[i].len_dec)+"')"
        ##print(attributtesInsert)
        queryInsert = '''INSERT INTO structureDB VALUES {0} '''.format(attributtesInsert)
        ##print("Consulta que se realizo para Insertar")
        ##print(queryInsert)
        c.execute(queryInsert)
        ##print("OK-->>Insert")
    conn.commit()
    c.close()
    conn.close()



def obtain_dataset():
    #print("Base de Datos")
    conn = sqlite3.connect('PREPRODAT.db')
    #print("Database created and opened succesfully")
    c = conn.cursor()
    list_dataset = []
    try:
        #print("Entrrrrrrrroooooo")
        #print("Entrrrrrrrroooooo")
        c.execute('''SELECT * FROM datasetDB''')
        #x = c.fetchone()
        for row in c.fetchall():
            listTupla = []
            for column in range(0, len(row)):
                listTupla.append(row[column])
            list_dataset.append(listTupla)
        conn.commit()
        c.close()
        conn.close()
    except Exception:
        #print("La base de datos no existe")
        msg("La base de datos no existe")
    return np.array(list_dataset)


def obtain_structure():
    #print("Base de Datos")
    conn = sqlite3.connect('PREPRODAT.db')
    #print("Database created and opened succesfully")
    c = conn.cursor()
    list_dataset = []
    try:
        #print("Entrrrrrrrroooooo")
        #print("Entrrrrrrrroooooo")
        c.execute('''SELECT * FROM structureDB''')
        #x = c.fetchone()
        for row in c.fetchall():
            listTupla = []
            for column in range(0, len(row)):
                listTupla.append(row[column])
            list_dataset.append(listTupla)
        conn.commit()
        c.close()
        conn.close()
    except Exception:
        #print("La base de datos no existe")
        msg("La base de datos no existe")
    return np.array(list_dataset)

def obtain_name_column(option):
    #print("Base de Datos")
    conn = sqlite3.connect('PREPRODAT.db')
    #print("Database created and opened succesfully")
    c = conn.cursor()
    list_dataset = []
    try:
        #Cuando el parametro option es igual a 0 devuelve el nombre de todas las columnas
        if option==0:

            c.execute('''SELECT Field_name FROM structureDB''')
            for row in c.fetchall():
                for column in range(0, len(row)):
                    list_dataset.append(row[column])
        else:

            c.execute('''SELECT Field_name FROM structureDB WHERE Field_type='C' ''')
            for row in c.fetchall():
                for column in range(0, len(row)):
                    list_dataset.append(row[column])

        conn.commit()
        c.close()
        conn.close()
    except Exception:
        #print("La base de datos no existe")
        msg("La base de datos no existe")
    #print(np.array(list_dataset))
    return np.array(list_dataset)



def obtain_categorical_columns():
    #print("Base de Datos")
    conn = sqlite3.connect('PREPRODAT.db')
    #print("Database created and opened succesfully")
    c = conn.cursor()
    list_dataset = []
    try:
        c.execute('''SELECT Number FROM structureDB WHERE Field_type="C"''')
        for row in c.fetchall():
            for column in range(0, len(row)):
                list_dataset.append(row[column])
        conn.commit()
        c.close()
        conn.close()
    except Exception:
        print("La base de datos no existe")
        #msg("La base de datos no existe")
    #print(np.array(list_dataset))
    return np.array(list_dataset)

def obtain_numeric_columns():
    #print("Base de Datos")
    conn = sqlite3.connect('PREPRODAT.db')
    #print("Database created and opened succesfully")
    c = conn.cursor()
    list_dataset = []
    try:
        c.execute('''SELECT Number FROM structureDB WHERE Field_type="N"''')
        for row in c.fetchall():
            for column in range(0, len(row)):
                list_dataset.append(row[column])
        conn.commit()
        c.close()
        conn.close()
    except Exception:
        #print("La base de datos no existe")
        msg("La base de datos no existe")
    #print(np.array(list_dataset))
    return np.array(list_dataset)

def obtain_date_columns():
    #print("Base de Datos")
    conn = sqlite3.connect('PREPRODAT.db')
    #print("Database created and opened succesfully")
    c = conn.cursor()
    list_dataset = []
    try:
        c.execute('''SELECT Number FROM structureDB WHERE Field_type="D"''')
        for row in c.fetchall():
            for column in range(0, len(row)):
                list_dataset.append(row[column])
        conn.commit()
        c.close()
        conn.close()
    except Exception:
        #print("La base de datos no existe")
        msg("La base de datos no existe")
    #print(np.array(list_dataset))
    return np.array(list_dataset)


def update_data_type_column_categorical_to_numerics():
    #print("Base de Datos")
    conn = sqlite3.connect('PREPRODAT.db')
    #print("Database created and opened succesfully")
    c = conn.cursor()

    try:

        c.execute('''UPDATE structureDB SET Field_type = 'N' , length = '8', len_dec='5' WHERE Field_type ='C' ''')
        conn.commit()
        c.close()
        conn.close()
        #print("Updated type data of the columns")
    except Exception:
        #print("Error al Actualizar")
        msg("Error al actualizar")

def update_data_type_column_date_to_numerics():
    #print("Base de Datos")
    conn = sqlite3.connect('PREPRODAT.db')
    #print("Database created and opened succesfully")
    c = conn.cursor()

    try:

        c.execute('''UPDATE structureDB SET Field_type ='N', length = '8', len_dec='5' WHERE Field_type ='D' ''')
        conn.commit()
        c.close()
        conn.close()
        #print("Updated type data of the columns")
    except Exception:
        #print("Error al Actualizar")
        msg("Error al actualizar")




def deletedata ():
    conn = sqlite3.connect('PREPRODAT.db')
    #print("Database created and opened succesfully")
    c = conn.cursor()
    c.execute('''DELETE FROM datasetDB''')
    conn.commit()
    c.close()
    conn.close()

def deleteTableStructure():
    conn = sqlite3.connect('PREPRODAT.db')
    #print("Database created and opened succesfully")
    c = conn.cursor()
    c.execute('''drop table if exists structureDB''')
    conn.commit()
    c.close()
    conn.close()

def deleteTableDataset():
    conn = sqlite3.connect('PREPRODAT.db')
    #print("Database created and opened succesfully")
    c = conn.cursor()
    c.execute('''drop table if exists datasetDB''')
    conn.commit()
    c.close()
    conn.close()

def delete_element_structure(number_column):
    listComplete = []
    structure = obtain_structure()
    structure = np.delete(structure, number_column,0)
    structure = np.delete(structure, 0, 1)
    deleteTableStructure()
    #print("Estructuraaaaa")
    #print(structure)
    for x in range(0, structure.shape[0]):
        field_name = structure[x,0]
        field_type = structure[x,1]
        field_len = structure[x,2]
        field_len_dec = structure[x,3]
        objTemp = Metadatos(field_name, field_type, field_len, field_len_dec)
        listComplete.append(objTemp)
    #print(listComplete)
    insert_metadatos(listComplete)



def msg(self, text):
	msg = QMessageBox()
	msg.setIcon(QMessageBox.Information)
	msg.setText(text)
	# msg.setInformativeText("This is additional information")
	msg.setWindowTitle("Information")
	msg.setDetailedText("The details are as follows:")
	msg.setStandardButtons(QMessageBox.Ok)
	msg.exec_()

def msg(text):
	msg = QMessageBox()
	msg.setIcon(QMessageBox.Information)
	msg.setText(text)
	# msg.setInformativeText("This is additional information")
	msg.setWindowTitle("Information")
	msg.setDetailedText("The details are as follows:")
	msg.setStandardButtons(QMessageBox.Ok)
	msg.exec_()

#Objecto para manejar los meta datos y los datos
class Datastructure:
    metadatos = np.array([])
    dataset = np.array([])
    def __init__(self, categorical, numerics, date, field_name,dataset):
        self.categorical = categorical
        self.numerics = numerics
        self.date = date
        self.field_name = field_name
        self.dataset = dataset

class Metadatos:
    field_name = ""
    field_type = ""
    length = ""
    len_dec = ""
    def __init__(self, field_name, field_type, length, len_dec):
        self.field_name = field_name
        self.field_type = field_type
        self.length = length
        self.len_dec = len_dec










