import numpy as np
from sklearn.decomposition import PCA

def pca (data, n_component):
    pca = PCA(copy=True, iterated_power='auto', n_components=n_component, random_state=None, svd_solver='full', tol = 0.0, whiten=False)
    pca.fit(data.astype(float))
    newdata = pca.fit_transform(data.astype(float))

    return newdata, pca.get_covariance(), pca.get_precision(), pca.mean_, pca.explained_variance_, pca.n_components



