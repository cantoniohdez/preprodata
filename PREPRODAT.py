# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'PREPRODATv2.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui
import scale
import stabilize
import data_views
import convertoJulianDate
from PyQt4.QtGui import *
import numpy as np
import manage_database
from sklearn import preprocessing
import stadistical_categorical_encode
import pearson_correlation_view
import missing_value_view
import pca_view
import sampling_view
import chi_square_view
from sklearn.ensemble import IsolationForest
from sklearn.neighbors import LocalOutlierFactor

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Form(object):

    def setupUi(self, Form):
        Form.setObjectName(_fromUtf8("Form"))
        Form.resize(549, 800)
        self.frame = QtGui.QFrame(Form)
        self.frame.setGeometry(QtCore.QRect(50, 10, 441, 111))
        self.frame.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtGui.QFrame.Raised)
        self.frame.setObjectName(_fromUtf8("frame"))
        self.pushButton_4 = QtGui.QPushButton(self.frame)
        self.pushButton_4.setGeometry(QtCore.QRect(110, 40, 251, 27))
        self.pushButton_4.setObjectName(_fromUtf8("pushButton_4"))
        self.pushButton_4.clicked.connect(self.showLoadFile)
        self.pushButton = QtGui.QPushButton(self.frame)
        self.pushButton.setGeometry(QtCore.QRect(110, 10, 251, 27))
        self.pushButton.setObjectName(_fromUtf8("pushButton"))
        self.pushButton.clicked.connect(self.showLoadFile)
        self.pushButton_15 = QtGui.QPushButton(self.frame)
        self.pushButton_15.setGeometry(QtCore.QRect(110, 70, 251, 27))
        self.pushButton_15.setObjectName(_fromUtf8("pushButton_15"))
        self.pushButton_15.clicked.connect(self.savefile)
        self.frame_2 = QtGui.QFrame(Form)
        self.frame_2.setGeometry(QtCore.QRect(50, 120, 441, 181))
        self.frame_2.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame_2.setFrameShadow(QtGui.QFrame.Raised)
        self.frame_2.setObjectName(_fromUtf8("frame_2"))
        self.label = QtGui.QLabel(self.frame_2)
        self.label.setGeometry(QtCore.QRect(100, 10, 171, 17))
        self.label.setObjectName(_fromUtf8("label"))
        self.pushButton_6 = QtGui.QPushButton(self.frame_2)
        self.pushButton_6.setGeometry(QtCore.QRect(110, 120, 251, 27))
        self.pushButton_6.setObjectName(_fromUtf8("pushButton_6"))
        self.pushButton_6.clicked.connect(self.discretizacion_main)
        self.pushButton_2 = QtGui.QPushButton(self.frame_2)
        self.pushButton_2.setGeometry(QtCore.QRect(110, 30, 251, 27))
        self.pushButton_2.setObjectName(_fromUtf8("pushButton_2"))
        self.pushButton_2.clicked.connect(self.showScale)
        self.pushButton_5 = QtGui.QPushButton(self.frame_2)
        self.pushButton_5.setGeometry(QtCore.QRect(110, 90, 251, 27))
        self.pushButton_5.setObjectName(_fromUtf8("pushButton_5"))
        self.pushButton_3 = QtGui.QPushButton(self.frame_2)
        self.pushButton_3.setGeometry(QtCore.QRect(110, 60, 251, 27))
        self.pushButton_3.setObjectName(_fromUtf8("pushButton_3"))
        self.pushButton_3.clicked.connect(self.showStabilize)
        self.pushButton_17 = QtGui.QPushButton(self.frame_2)
        self.pushButton_17.setGeometry(QtCore.QRect(110, 150, 251, 27))
        self.pushButton_17.setObjectName(_fromUtf8("pushButton_17"))
        self.pushButton_17.clicked.connect(self.showJulian)
        self.label.raise_()
        self.pushButton_6.raise_()
        self.pushButton_2.raise_()
        self.pushButton_5.raise_()
        self.pushButton_3.raise_()
        self.pushButton_17.raise_()
        self.label.raise_()
        self.pushButton_6.raise_()
        self.pushButton_2.raise_()
        self.pushButton_5.raise_()
        self.pushButton_3.raise_()
        self.frame_3 = QtGui.QFrame(Form)
        self.frame_3.setGeometry(QtCore.QRect(50, 300, 441, 111))
        self.frame_3.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame_3.setFrameShadow(QtGui.QFrame.Raised)
        self.frame_3.setObjectName(_fromUtf8("frame_3"))
        self.pushButton_8 = QtGui.QPushButton(self.frame_3)
        self.pushButton_8.setGeometry(QtCore.QRect(110, 60, 251, 27))
        self.pushButton_8.setObjectName(_fromUtf8("pushButton_8"))
        self.pushButton_8.clicked.connect(self.showPCA_View)
        self.label_2 = QtGui.QLabel(self.frame_3)
        self.label_2.setGeometry(QtCore.QRect(100, 10, 171, 17))
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.pushButton_7 = QtGui.QPushButton(self.frame_3)
        self.pushButton_7.setGeometry(QtCore.QRect(110, 30, 251, 27))
        self.pushButton_7.setObjectName(_fromUtf8("pushButton_7"))
        self.pushButton_7.clicked.connect(self.showSampling)
        self.frame_4 = QtGui.QFrame(Form)
        self.frame_4.setGeometry(QtCore.QRect(50, 410, 441, 161))
        self.frame_4.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame_4.setFrameShadow(QtGui.QFrame.Raised)
        self.frame_4.setObjectName(_fromUtf8("frame_4"))
        self.label_4 = QtGui.QLabel(self.frame_4)
        self.label_4.setGeometry(QtCore.QRect(100, 10, 171, 17))
        self.label_4.setObjectName(_fromUtf8("label_4"))
        self.pushButton_9 = QtGui.QPushButton(self.frame_4)
        self.pushButton_9.setGeometry(QtCore.QRect(110, 30, 251, 27))
        self.pushButton_9.setObjectName(_fromUtf8("pushButton_9"))
        self.pushButton_9.clicked.connect(self.showMissingValues)
        #self.pushButton_10 = QtGui.QPushButton(self.frame_4)
        #self.pushButton_10.setGeometry(QtCore.QRect(110, 90, 251, 2))
        #self.pushButton_10.setObjectName(_fromUtf8("pushButton_10"))
        #self.pushButton_10.clicked.connect(self.delete_outliers_lof)
        self.pushButton_11 = QtGui.QPushButton(self.frame_4)
        self.pushButton_11.setGeometry(QtCore.QRect(110, 60, 251, 27))
        self.pushButton_11.setObjectName(_fromUtf8("pushButton_11"))
        self.pushButton_11.clicked.connect(self.delete_outliers_isolation_forest)
        self.frame_5 = QtGui.QFrame(Form)
        self.frame_5.setGeometry(QtCore.QRect(50, 570, 441, 160))
        self.frame_5.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame_5.setFrameShadow(QtGui.QFrame.Raised)
        self.frame_5.setObjectName(_fromUtf8("frame_5"))
        self.pushButton_5.clicked.connect(self.stadistical_categorical_encoding)
        self.label_3 = QtGui.QLabel(self.frame_5)
        self.label_3.setGeometry(QtCore.QRect(100, 10, 171, 17))
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.pushButton_12 = QtGui.QPushButton(self.frame_5)
        self.pushButton_12.setGeometry(QtCore.QRect(110, 30, 251, 27))
        self.pushButton_12.setObjectName(_fromUtf8("pushButton_12"))
        self.pushButton_12.clicked.connect(self.showChi_square)
        self.pushButton_13 = QtGui.QPushButton(self.frame_5)
        self.pushButton_13.setGeometry(QtCore.QRect(110, 60, 251, 27))
        self.pushButton_13.setObjectName(_fromUtf8("pushButton_13"))
        self.pushButton_13.clicked.connect(self.pearson_correlation_view)
        #self.pushButton_14 = QtGui.QPushButton(self.frame_5)
        #self.pushButton_14.setGeometry(QtCore.QRect(110, 90, 251, 27))
        #self.pushButton_14.setObjectName(_fromUtf8("pushButton_14"))
        self.frame_6 = QtGui.QFrame(Form)
        self.frame_6.setGeometry(QtCore.QRect(50, 729, 441, 50))
        self.frame_6.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame_6.setFrameShadow(QtGui.QFrame.Raised)
        self.frame_6.setObjectName(_fromUtf8("frame_6"))
        self.pushButton_16 = QtGui.QPushButton(self.frame_6)
        self.pushButton_16.setGeometry(QtCore.QRect(110, 10, 251, 27))
        self.pushButton_16.setObjectName(_fromUtf8("pushButton_16"))
        self.pushButton_16.clicked.connect(self.close_all_windows)

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        Form.setWindowTitle(_translate("Form", "PREPRODATA", None))
        self.pushButton_4.setText(_translate("Form", "View Current preprocessed file", None))
        self.pushButton.setText(_translate("Form", "Load File", None))
        self.pushButton_15.setText(_translate("Form", "Save File", None))
        self.label.setText(_translate("Form", "Data Transformation", None))
        self.pushButton_6.setText(_translate("Form", "Binary Categorical Encoding", None))
        self.pushButton_2.setText(_translate("Form", "Scale", None))
        self.pushButton_5.setText(_translate("Form", "Statistical Categorical Encoding", None))
        self.pushButton_3.setText(_translate("Form", "Stabilize", None))
        self.pushButton_8.setText(_translate("Form", "PCA", None))
        self.pushButton_17.setText(_translate("Form", "Converter to Julian Date", None))
        self.label_2.setText(_translate("Form", "Data Reduction", None))
        self.pushButton_7.setText(_translate("Form", "Sampling", None))
        self.label_4.setText(_translate("Form", "Data Cleaning", None))
        self.pushButton_9.setText(_translate("Form", "Imputation of Missing Values", None))
        #self.pushButton_10.setText(_translate("Form", "Outliers - LOF", None))
        self.pushButton_11.setText(_translate("Form", "Detection of Outliers Values", None))
        self.label_3.setText(_translate("Form", "Data Integration", None))
        self.pushButton_12.setText(_translate("Form", "Chi-Square Test", None))
        self.pushButton_13.setText(_translate("Form", "Pearson Correlation", None))
        #self.pushButton_14.setText(_translate("Form", "PushButton7", None))
        self.pushButton_16.setText(_translate("Form", "End Program", None))

    def showScale(self):
        self.FormScale = QtGui.QWidget()
        self.ui = scale.Ui_FormScale()
        self.ui.setupUi(self.FormScale)
        self.FormScale.show()

    def showPCA_View(self):
        self.FormPCA = QtGui.QWidget()
        self.ui = pca_view.Ui_FormPCA()
        self.ui.setupUi(self.FormPCA)
        self.FormPCA.show()

    def showChi_square(self):
        numero_categorical_column = manage_database.obtain_categorical_columns()
        if len(numero_categorical_column) == 0:
            self.msg("There are not categorical data for analyze")
        else:
            self.FormChiSquare = QtGui.QWidget()
            self.ui = chi_square_view.Ui_FormChiSquare()
            self.ui.setupUi(self.FormChiSquare)
            self.FormChiSquare.show()

    def showJulian(self):
        numero_date_column = manage_database.obtain_date_columns()
        if len(numero_date_column) == 0:
            self.msg("There are not date data for convert")
        else:
            self.FormJulian = QtGui.QWidget()
            self.ui = convertoJulianDate.Ui_FormJulian()
            self.ui.setupUi(self.FormJulian)
            self.FormJulian.show()

    def showStabilize(self):
        self.FormStabilize = QtGui.QWidget()
        self.ui = stabilize.Ui_FormStabilize()
        self.ui.setupUi(self.FormStabilize)
        self.FormStabilize.show()

    def showMissingValues(self):
        self.FormMissingValue = QtGui.QWidget()
        self.ui = missing_value_view.Ui_FormMissingValue()
        self.ui.setupUi(self.FormMissingValue)
        self.FormMissingValue.show()

    def showLoadFile(self):
        self.FormDataViews = QtGui.QWidget()
        self.ui = data_views.Ui_FormDataViews()
        self.ui.setupUi(self.FormDataViews)
        self.FormDataViews.show()

    def showSampling(self):
        self.FormSampling = QtGui.QWidget()
        self.ui = sampling_view.Ui_FormSampling()
        self.ui.setupUi(self.FormSampling)
        self.FormSampling.show()

    def savefile(self):
        dataset = manage_database.obtain_dataset()
        #print("SAVEEEEEE")
        #print(dataset)
        dir_save = QFileDialog().getSaveFileName()
        temp = dir_save.split(".")
        if str(temp[-1]) == "txt":
            np.savetxt(dir_save, dataset, delimiter='\t', fmt="%s")
        else:
            np.savetxt(dir_save+str(".txt"), dataset, delimiter='\t', fmt="%s")
        #manage_database.deleteDB()

    def close_all_windows(self):
        manage_database.deleteTableDataset()
        manage_database.deleteTableStructure()
        app = QtGui.QApplication.instance()
        app.closeAllWindows()

    def arrayString_to_arrayFloat(self,dataset):
        list_dataset = []
        for i in range(0, dataset.shape[0]):
            list_temp =[]
            for j in range(0,dataset.shape[1]):
                list_temp.append(float(dataset[i,j]))
            list_dataset.append(list_temp)
        return np.array(list_dataset)


    def stadistical_categorical_encoding(self):
        numero_categorical_column = manage_database.obtain_categorical_columns()
        if len(numero_categorical_column)==0:
            self.msg("There are not categorical data for encoding")
        else:
            self.FormStadisticalCategorical= QtGui.QWidget()
            self.ui = stadistical_categorical_encode.Ui_FormStadistical_Categorical_Encode()
            self.ui.setupUi(self.FormStadisticalCategorical)
            self.FormStadisticalCategorical.show()

    def pearson_correlation_view(self):
        numero_numeric_column = manage_database.obtain_numeric_columns()
        numero_categorical_column = manage_database.obtain_categorical_columns()
        if len(numero_numeric_column)<=1:
            self.msg("There are not two or more numeric data")
        elif len(numero_categorical_column)!= 0:
            self.msg("There is(are) {0} categorical column(s)".format(len(numero_categorical_column)))
            self.FormPearsonCorrelationView = QtGui.QWidget()
            self.ui = pearson_correlation_view.Ui_FormPearsonCorrelation()
            self.ui.setupUi(self.FormPearsonCorrelationView)
            self.FormPearsonCorrelationView.show()
        else:
            self.FormPearsonCorrelationView= QtGui.QWidget()
            self.ui = pearson_correlation_view.Ui_FormPearsonCorrelation()
            self.ui.setupUi(self.FormPearsonCorrelationView)
            self.FormPearsonCorrelationView.show()

    def delete_outliers_isolation_forest(self):
        try:
            dataset = manage_database.obtain_dataset()
            print(dataset)
            print(dataset.size)
            rng = np.random.RandomState(42)
            clf = IsolationForest(max_samples=100, random_state=rng)
            clf.fit(dataset.astype(float))
            outliers_tuples = clf.predict(dataset)
            print(outliers_tuples)
            newdataset = []
            count = 0
            for x in range(0, len(outliers_tuples)):
                if outliers_tuples[x]==1:
                    newdataset.append(dataset[x])
                else:
                    count+=1

            manage_database.insert_dataset(np.array(newdataset))
            self.msg("Successful Operation, {0} outliers values detected".format(count))
        except Exception:
            self.msg("Error in the operation")

    def delete_outliers_lof(self):
        try:
            dataset = manage_database.obtain_dataset()
            print("1111")
            clf = LocalOutlierFactor(n_neighbors=20)
            print("22222")
            clf.fit(dataset)
            print("33333")
            outliers_tuples = clf.predict(dataset.dtype(float))
            print("4444")
            print(outliers_tuples)
            newdataset = []
            count = 0
            for x in range(0, len(outliers_tuples)):
                if outliers_tuples[x]==1:
                    newdataset.append(dataset[x])
                else:
                    count+=1

            manage_database.insert_dataset(np.array(newdataset))
            self.msg("Successful Operation, {0} outliers values detected".format(count))
        except Exception:
            self.msg("Error in the operation")


    def discretizacion_main(self):
        dataset = manage_database.obtain_dataset()
        datasetTemp = dataset.T.tolist()
        list_column = manage_database.obtain_categorical_columns()
        name_column = manage_database.obtain_name_column(1)
        structure = manage_database.obtain_structure()
        #print("Estructura Actual")
        #print(structure)

        name_column_result = []


        if len(list_column) !=0:
            lista_completa = []

            for v in range(len(list_column),0,-1):
                #print("Value v")
                #print(list_column[int(v)-1])
                datasetTemp = (np.delete(np.array(datasetTemp), list_column[int(v)-1], axis=0)).tolist()
                structure = (np.delete(np.array(structure), list_column[int(v)-1], axis=0)).tolist()

            #print("Estructura despues de eliminar")
            #print(structure)

            #print("DAtaset temporal antes de comenzar")

            #print(np.array(datasetTemp).T)
            #print(np.array(datasetTemp).T.shape)

            for z in range(0, len(list_column)):
                #print("valores de z")
                #print(z)
                zzz = self.discretizacion(dataset,list_column[int(z)])
                for k in range(0,len(zzz)):
                    datasetTemp.insert(len(datasetTemp), zzz[k])
                    structure.insert(len(structure), ['',str(name_column[int(z)])+"_"+str(k), 'N', '2', '0'])
                    name_column_result.append(str(name_column[int(z)])+"_"+str(k))


            structure = np.delete(np.array(structure), 0, axis= 1)
            #print("Structure Final")
            list_metadatos = []
            for tuple in structure:
                objT = manage_database.Metadatos(str(tuple[0]),str(tuple[1]),str(tuple[2]),str(tuple[3]))
                list_metadatos.append(objT)
            manage_database.deleteTableStructure()
            manage_database.insert_metadatos(list_metadatos)
            #print(structure)
            #print(structure[0])
            #print(structure[1])
            x = structure[0]
            #print(x[0])


            #print("Name Columnas Resultantes")
            #print(name_column_result)
            #print("dataset")
            dataset_end = np.array(datasetTemp).T


            #np.concatenate(dataset,lista_completanp[0])

            manage_database.insert_dataset(dataset_end)
            self.msg("Successful Operation")

        else:
          self.msg("There are not categorical columns")
        #print("Fin del Algoritmo para la transformacion de las variables categoricas")

    def discretizacion(self,dataset, nro_columna):
        #print("Dataset")
        #print(dataset)
        columna_categorica = dataset[:, int(nro_columna)]
        #print("Columna Categorica")
        #print(columna_categorica)
        categorias = []
        for i in columna_categorica:
            if i not in categorias:
                categorias.append(i)
        categorias.sort()
        list_final = []
        for x in range(0, len(categorias)):
            list_temp = []
            for y in range(0, len(columna_categorica)):
                if columna_categorica[y] == categorias[x]:
                    list_temp.append(1)
                else:
                    list_temp.append(0)
            list_final.append(list_temp)
        return list_final

    def msg(self, text):
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Information)
        msg.setText(text)
        #msg.setInformativeText("This is additional information")
        msg.setWindowTitle("Information")
        msg.setDetailedText("The details are as follows:")
        msg.setStandardButtons(QMessageBox.Ok)
        msg.exec_()

        
if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    Form = QtGui.QWidget()
    ui = Ui_Form()
    ui.setupUi(Form)
    Form.show()
    sys.exit(app.exec_())

