# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'stadistical_categorical_encode.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui


try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_FormStadistical_Categorical_Encode(object):

    def setupUi(self, Form):
        Form.setObjectName(_fromUtf8("Form"))
        Form.resize(661, 708)
        self.frame = QtGui.QFrame(Form)
        self.frame.setGeometry(QtCore.QRect(10, 10, 641, 231))
        self.frame.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtGui.QFrame.Raised)
        self.frame.setObjectName(_fromUtf8("frame"))
        self.label = QtGui.QLabel(self.frame)
        self.label.setGeometry(QtCore.QRect(20, 40, 231, 17))
        self.label.setObjectName(_fromUtf8("label"))
        self.label_2 = QtGui.QLabel(self.frame)
        self.label_2.setGeometry(QtCore.QRect(60, 80, 191, 17))
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.label_3 = QtGui.QLabel(self.frame)
        self.label_3.setGeometry(QtCore.QRect(150, 120, 101, 21))
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.label_4 = QtGui.QLabel(self.frame)
        self.label_4.setGeometry(QtCore.QRect(240, 10, 141, 17))
        self.label_4.setObjectName(_fromUtf8("label_4"))
        self.lineEdit = QtGui.QLineEdit(self.frame)
        self.lineEdit.setGeometry(QtCore.QRect(270, 40, 113, 27))
        self.lineEdit.setObjectName(_fromUtf8("lineEdit"))
        self.lineEdit_2 = QtGui.QLineEdit(self.frame)
        self.lineEdit_2.setGeometry(QtCore.QRect(270, 80, 113, 27))
        self.lineEdit_2.setObjectName(_fromUtf8("lineEdit_2"))
        self.lineEdit_3 = QtGui.QLineEdit(self.frame)
        self.lineEdit_3.setGeometry(QtCore.QRect(270, 120, 113, 27))
        self.lineEdit_3.setObjectName(_fromUtf8("lineEdit_3"))
        self.label_5 = QtGui.QLabel(self.frame)
        self.label_5.setGeometry(QtCore.QRect(390, 40, 171, 17))
        self.label_5.setObjectName(_fromUtf8("label_5"))
        self.pushButton = QtGui.QPushButton(self.frame)
        self.pushButton.setGeometry(QtCore.QRect(500, 190, 99, 27))
        self.pushButton.setObjectName(_fromUtf8("pushButton"))
        self.pushButton.clicked.connect(self.encode)
        self.pushButton_2 = QtGui.QPushButton(self.frame)
        self.pushButton_2.setGeometry(QtCore.QRect(40, 190, 99, 27))
        self.pushButton_2.setObjectName(_fromUtf8("pushButton_2"))
        self.scrollArea = QtGui.QScrollArea(Form)
        self.scrollArea.setGeometry(QtCore.QRect(10, 280, 641, 161))
        self.scrollArea.setWidgetResizable(True)
        self.scrollArea.setObjectName(_fromUtf8("scrollArea"))
        self.scrollAreaWidgetContents = QtGui.QWidget()
        self.scrollAreaWidgetContents.setGeometry(QtCore.QRect(0, 0, 639, 159))
        self.scrollAreaWidgetContents.setObjectName(_fromUtf8("scrollAreaWidgetContents"))
        self.scrollArea.setWidget(self.scrollAreaWidgetContents)
        self.label_6 = QtGui.QLabel(Form)
        self.label_6.setGeometry(QtCore.QRect(20, 260, 241, 16))
        self.label_6.setObjectName(_fromUtf8("label_6"))
        self.label_7 = QtGui.QLabel(Form)
        self.label_7.setGeometry(QtCore.QRect(20, 460, 141, 17))
        self.label_7.setObjectName(_fromUtf8("label_7"))
        self.tableWidget = QtGui.QTableWidget(Form)
        self.tableWidget.setGeometry(QtCore.QRect(10, 480, 641, 211))
        self.tableWidget.setObjectName(_fromUtf8("tableWidget"))
        self.tableWidget.setColumnCount(0)
        self.tableWidget.setRowCount(0)

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)
        regexp = QtCore.QRegExp('^([01]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])$')
        validator = QtGui.QRegExpValidator(regexp)
        self.lineEdit_2.setValidator(validator)

        regexp1 = QtCore.QRegExp('^[2-3]|2[2-9]|3[0-6]$')
        validator = QtGui.QRegExpValidator(regexp1)
        self.lineEdit.setValidator(validator)

        regexp2 = QtCore.QRegExp('^[0-9][0-9]|10[0-0]$')
        validator = QtGui.QRegExpValidator(regexp2)
        self.lineEdit_3.setValidator(validator)

    def retranslateUi(self, Form):
        Form.setWindowTitle(_translate("Form", "Stadistical Categorical Encoding", None))
        self.label.setText(_translate("Form", "Number of Samples in each mean", None))
        self.label_2.setText(_translate("Form", "Categories for chi-cuadrado", None))
        self.label_3.setText(_translate("Form", "Security Factor", None))
        self.label_4.setText(_translate("Form", "Input of algorithm", None))
        self.lineEdit.setText(_translate("Form", "36", None))
        self.lineEdit_2.setText(_translate("Form", "10", None))
        self.lineEdit_3.setText(_translate("Form", "20", None))
        self.label_5.setText(_translate("Form", "Values in Range: [22,36]", None))
        self.pushButton.setText(_translate("Form", "Encode", None))
        self.pushButton_2.setText(_translate("Form", "Cancel", None))
        self.label_6.setText(_translate("Form", "Values Resulting from coding", None))
        self.label_7.setText(_translate("Form", "Correlation Analysis", None))

    def encode(self):
        number_sample = int(self.lineEdit.text())
        chi_cuadrado = int(self.lineEdit_2.text())
        security_factor = int(self.lineEdit_3.text())
        info_text_area, matriz_correlacion = catEstadisticalEncoding(number_sample,chi_cuadrado,security_factor)

        #print("INterfaz Grafica")
        #print(info_text_area)
        #print(matriz_correlacion)



if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    Form = QtGui.QWidget()
    ui = Ui_FormStadistical_Categorical_Encode()
    ui.setupUi(Form)
    Form.show()
    sys.exit(app.exec_())

