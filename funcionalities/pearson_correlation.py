# -*- coding: utf-8 -*-
import math
import random
import numpy as np
import pandas as pd
import manage_database

class Correlation_Values:
    value_of_categories_original = []
    value_random_of_categories = []
    value_of_corr = 0

    def __init__(self,value_of_categories_original,value_random_of_categories,value_of_corr):
        self.value_of_categories_original = value_of_categories_original
        self.value_random_of_categories = value_random_of_categories
        self.value_of_corr = value_of_corr


class Format_Result_Cat_Stadistical:
    number_of_column_categorical = []
    value_categoria_inicial = []
    value_categoria_final = []
    valor_correlation_max = []

    def __init__(self, number, initial, end, value_corre):
        self.number_of_column_categorical =number
        self.value_categoria_inicial = initial
        self.value_categoria_final =end
        self.valor_correlation_max = value_corre



def pearson_correlations(dataset, var):
    #Si el valor de var=0 obtenemos todas las correlaciones de todas las variables del dataset
    correlations = []
    ##print(dataset.shape[0])

    if var == 0:
        for x in range(0, dataset.shape[0]):
            value_correlation = []
            for y in range(0, dataset.shape[0]):
                value = value_pearson_correlation(dataset[x],dataset[y])
                ##print(value)
                value_correlation.append(value)
            correlations.append(value_correlation)

        return np.array(correlations)
    else:
        # El valor de var va a determinar la variable de la cual se quiere determinar la correlacion
        #print("Aqui va el codigo para cuando solo necesito la correlacion de una columna contra todas las demas")
        return np.array([])


def value_pearson_correlation(column1, column2):
    #R = (N*SumXY-SumX*SumY)/RAIZ{[N*Sum(X^2)-(SumX)^2]*[N*Sum(Y^2)-(SumY)^2]}
    #column1 = np.array([1,2,3,4,5,6,7,8,9,10])
    #column2 = np.array([2,4,6,8,10,12,5,16,18,20])
    R = 0
    N = len(column1)
    SumXY = 0
    SumX = 0
    SumY = 0
    SumX2 = 0
    SumY2 = 0
    for x in range(0,N):
        SumXY+= float(column1[x])*float(column2[x])
        SumX+= float(column1[x])
        SumY+= float(column2[x])
        SumX2+= math.pow(float(column1[x]),2)
        SumY2+= math.pow(float(column2[x]),2)
    R = float(((N*SumXY)-(SumX*SumY)))/float((math.sqrt(((N*SumX2)-math.pow(SumX,2)))*math.sqrt((N*SumY2)-math.pow(SumY,2))))
    return R



#Algoritmo para asignar valores numericos a las categorias
#Parametros
# 1- Dataset - Conjunto de datos.
# 2- listCat - Lista que contiene la posicion de las variables categoricas.
def catEstadisticalEncoding(muestras_en_cada_media, chi_cuadrado, factor_seguridad):
    dataset = manage_database.obtain_dataset()
    listCategories_var = manage_database.obtain_categorical_columns()
    resumen_of_result = []
    for position_column in listCategories_var:
        promedio = []
        datasetTemp = dataset
        historial_of_value_correlation_for_end = []
        ##print("Variable categorica en la columna {0}").format(position_column)
        for value1 in range (0,int(5*chi_cuadrado*factor_seguridad)):
            ##print("Iteracion {0}".format(value1))
            historial_of_value_correlation_for_var = []
            for value2 in range(0,muestras_en_cada_media):
                #Obtengo los valores de las columnas categoricas
                columnDataset = datasetTemp[:,position_column]

                #Obtengo los valores que toma la variable categorica
                listCategories = delete_duplicados(columnDataset)
                ##print("Lista de Categorias")
                ##print(listCategories)

                #For deberia ir aqui
                listCategoriesRandom = generate_values_categories(listCategories)
                ##print("Lista de Valores Aleatorios")
                ##print(listCategoriesRandom)

                columnDataset_Random = change_Categories_to_Categories_Random(columnDataset, listCategories,listCategoriesRandom )
                ##print("Valores Aleatorios de como quedan las columnas")
                ##print(columnDataset_Random)

                datasetNew = add_column_random_to_dataset(datasetTemp, columnDataset_Random, position_column)
                ##print("Dataset con las variables categoricas con valores Aleatorios")
                ##print(datasetNew)

                column_selected = myrandomInt(datasetNew.shape[1], position_column)
                ##print("Columna seleccionada para realizar el analisis del correlacion")
                ##print(column_selected)

                if isCategorical(column_selected, listCategories_var)==True:
                    ##print("XXX")
                    # Obtengo los valores de las columnas categoricas
                    columnDataset2 = datasetTemp[:, column_selected]

                    # Obtengo los valores que toma la variable categorica
                    listCategories2 = delete_duplicados(columnDataset2)
                    # #print("Lista de Categorias")
                    # #print(listCategories)

                    # For deberia ir aqui
                    listCategoriesRandom2 = generate_values_categories(listCategories2)
                    # #print("Lista de Valores Aleatorios")
                    # #print(listCategoriesRandom)

                    columnDataset_Random2 = change_Categories_to_Categories_Random(columnDataset2, listCategories2,
                                                                                  listCategoriesRandom2)
                    # #print("Valores Aleatorios de como quedan las columnas")
                    # #print(columnDataset_Random)

                    datasetNew2 = add_column_random_to_dataset(datasetNew, columnDataset_Random2, column_selected)
                    ##print("Dataset con las variables categoricas con valores Aleatorios")
                    ##print(datasetNew2)


                    ##print("Para CAlcular COrrelacion")
                    ##print(np.array(columnDataset_Random))
                    ##print("-------------------")
                    ##print(datasetNew2[:, column_selected])

                    value_correlation = value_pearson_correlation(np.array(columnDataset_Random),
                                                                  np.array(datasetNew2[:, column_selected]))
                    # #print("Valor de la correlacion entre la variable en analisis y la variable seleccionada aleatorioamente")
                    # #print(value_correlation)

                    objectTemp = Correlation_Values(listCategories, listCategoriesRandom, value_correlation)
                    historial_of_value_correlation_for_var.append(objectTemp)


                else:

                    ##print("YYY")
                    value_correlation = value_pearson_correlation(np.array(columnDataset_Random), np.array(datasetTemp[:,column_selected]))
                    ##print("Valor de la correlacion entre la variable en analisis y la variable seleccionada aleatorioamente")
                    ##print(value_correlation)

                    objectTemp = Correlation_Values(listCategories, listCategoriesRandom, value_correlation)
                    historial_of_value_correlation_for_var.append(objectTemp)
                    ##print("Valor dela Correlacion para la iteracion {0}".format(ggg))
                    ##print(historial_of_value_correlation_for_var[ggg].value_of_corr)

            #Busco el maximo valor de las correlaciones y entonces sustituyo los valores de las
            #categorias de la variable por las categorias aleatorias que resultaron tener mayor
            #valor de correlacion
            max_value_of_correlation = search_max_value_of_correlation(historial_of_value_correlation_for_var)
            datasetTemp = np.array(replace_column_dataset_with_value_correlation_max(datasetTemp, position_column,max_value_of_correlation.value_of_categories_original,max_value_of_correlation.value_random_of_categories))
            objectTempEnd = Correlation_Values(max_value_of_correlation.value_of_categories_original,max_value_of_correlation.value_random_of_categories,max_value_of_correlation.value_of_corr)
            historial_of_value_correlation_for_end.append(objectTempEnd)


            ##print(max_value_of_correlation.value_of_categories_original)
            ##print(max_value_of_correlation.value_random_of_categories)
            ##print(max_value_of_correlation.value_of_corr)

        max_value_of_correlationEnd = search_max_value_of_correlation(historial_of_value_correlation_for_end)
        ##print("Final")
        ##print(historial_of_value_correlation_for_end[0].value_of_categories_original)
        ##print(max_value_of_correlationEnd.value_of_categories_original)
        ##print(max_value_of_correlationEnd.value_random_of_categories)
        ##print(max_value_of_correlationEnd.value_of_corr)
        objectResultEnd = Format_Result_Cat_Stadistical(position_column, historial_of_value_correlation_for_end[0].value_of_categories_original,max_value_of_correlationEnd.value_random_of_categories,max_value_of_correlationEnd.value_of_corr)
        resumen_of_result.append(objectResultEnd)
    ##print(dataset)
    information_for_text_area = []
    #print("-------------------------------------------------------------------")
    information_for_text_area.append("------------------------------------------")
    for resultss in resumen_of_result:
        #print("La columna categorica es la numero {0}").format(resultss.number_of_column_categorical)
        information_for_text_area.append("La columna categorica es la numero: "+resultss.number_of_column_categorical)
        #print("Categorias originales del dataset {0}").format(resultss.value_categoria_inicial)
        information_for_text_area.append("La columna categorica es la numero: " + resultss.value_categoria_inicial)
        #print("Valores para las Categorias propuestos {0}").format(resultss.value_categoria_final)
        information_for_text_area.append("La columna categorica es la numero: " + resultss.valor_correlation_max)
        #print("El valores de correlación es {0}").format(resultss.valor_correlation_max)

        columnDataset_Random = change_Categories_to_Categories_Random(dataset[:,resultss.number_of_column_categorical], resultss.value_categoria_inicial,
                                                                      resultss.value_categoria_final)
        # #print("Valores Aleatorios de como quedan las columnas")
        # #print(columnDataset_Random)

        dataset = add_column_random_to_dataset(dataset, columnDataset_Random, resultss.number_of_column_categorical)
        # #print("Dataset con las variables categoricas con valores Aleatorios")
        #print("--------------------------------")
        information_for_text_area.append("------------------------------------------")
    manage_database.insert_dataset(dataset)


    correlation_matriz = pearson_correlations(dataset.T, 0)

    return information_for_text_area,correlation_matriz




#Dada la posicion de una columna y la lista de todas las columnas categoricas
#te devuelve el true en caso de que sea una de las columnas categoricas
def isCategorical(column, list_column_categorial):
    for x in list_column_categorial:
        if str(column) == str(x):
            return True
        else:
            return False



#Remplazo las categorias del Dataset de una columna por los valores aleatorios generados que tienen una
# correlacion maxima promedio
def replace_column_dataset_with_value_correlation_max(dataset, position_column, listCategories,listCategoriesRandom):
    columnDataset = dataset[:,position_column]
    columnDatasetNew = []
    for i in range(0, len(columnDataset)):
        for j in range(0, len(listCategories)):
            if str(columnDataset[i]) == str(listCategories[j]):
                columnDatasetNew.append(listCategoriesRandom[j])
    datasetNew = add_column_random_to_dataset(dataset, columnDatasetNew, position_column)
    return datasetNew

#Busca el maximo valor de correlacion y me devuelve las categorias originales
#Y los valores que lo hacen maximo
def search_max_value_of_correlation(historial_of_value_correlation_for_var):
    max_value = -2
    for x in range (0, len(historial_of_value_correlation_for_var)):
        if historial_of_value_correlation_for_var[x].value_of_corr > max_value:
            position_max = x
            max_value = historial_of_value_correlation_for_var[position_max].value_of_corr

    return historial_of_value_correlation_for_var[position_max]



#Voy a generar un valor aleatorio que sera la columna con la que realizare el analisis
#de correlacion le paso el maximo valor entero que puede tomar que seria el total de columnas
#del dataset y el valor excluyente que seria el valor que no puede ser que es la misma columna
#que estoy analizando
def myrandomInt(max, excluyente):
    x = random.randrange(0, max)
    for y in range(0,10):
        if x!=excluyente:
            break
        else:
            x = random.randrange(0, max)
    return x


#Le voy a pasar una lista de categorias y me retorna un valor aleatorio para
#cada una de esas
def generate_values_categories(listCategories):
    listCategoriesRandom = []
    for x in range(0, len(listCategories)):
        listCategoriesRandom.append(random.random())
    return listCategoriesRandom


#Le paso una columna categorica, la lista de categorias originales y la lista
# con valores aleatorios para ser sustituidos y me retorna una columna
#con los valores originales sustituidos por el valor aleatorio asignado
def change_Categories_to_Categories_Random(columnDataset, listCategories, listCategoriesRandom):
    columnDatasetNew = []
    for i in range(0, len(columnDataset)):
        for j in range(0, len(listCategories)):
            if columnDataset[i] == listCategories[j]:
                columnDatasetNew.append(listCategoriesRandom[j])
    return columnDatasetNew
#Le paso el dataset y una columna ademas de la position y me devuelve el dataset
#con los valores del datasetColumnRandom en la posicion que se le paso como parametro
def add_column_random_to_dataset(dataset, columnDataset_Random, position_column):
    #print("Info Column Dataset")
    #print(len(columnDataset_Random))
    #print("Info Dataset")
    #print(dataset.shape)
    x = np.delete(dataset, position_column, 1)
    #print("Info Dataset despues de eliminar")
    #print(x.shape)
    listTempDataset = []
    for k in range(0, x.shape[1]):
        if k == position_column:
            listTempDataset.append(columnDataset_Random)
            listTempDataset.append(x[:, k])
        else:
            listTempDataset.append(x[:, k])

    # Esto es para el caso en que la columna categorica sea la ultima
    if x.shape[1] == position_column:
        listTempDataset.append(columnDataset_Random)
    dataset = np.array(listTempDataset).T
    return dataset

#Aqui voy a calcular el promedio de correlacion
#dataset - Conjunto de Datos
#nroVariable - El valor de la posicion en donde esta la variable a la que le voy a realizar el calculo
#valoresCategoricos - Son los valores de las categorias de la variable antes mencionada
def extraerArrayAmaradoxValor(dataset, nroVariable, valoresCategorias):
    #print("Comienza las Iteracionesssssssss")
    #print(dataset)
    #print(nroVariable)
    #print(valoresCategorias)

    df = pd.DataFrame(dataset, dtype=float)

    for x in range(0, len(valoresCategorias)):
        #print("Listas amaradas para los diferentes valores de las categorias")
        #print(df.loc[df[nroVariable] == valoresCategorias[x]])
        list = np.array(df.loc[df[nroVariable] == valoresCategorias[x]])

        list1 = list[:,nroVariable]
        #print("Lista 1")
        #print(list1.T)
        list2 = np.delete(list,nroVariable,1)
        #print("Lista 2")
        #print(list2)
        #print("Example Plista 2")
        #print(list2[:,1])
        for y in range(0,list2.shape[1]):
            #print("ERORRRR")
            list_concat = np.concatenate([[list1],[list2[:,y]]],axis=0)
            list_concatT = list_concat.T
            #print(list_concatT)
            corre_value = pearson_correlations(list_concatT, 0)
            #print("Value of Correlations")
            #print(corre_value)
        break


def delete_duplicados(lista_original):
    lista_nueva = []
    for i in lista_original:
        if i not in lista_nueva:
            lista_nueva.append(i)
    return lista_nueva


#dataset = np.genfromtxt('/media/carlos/CarDat/Maestría/Tesis/Codificación/PREPRODAT/datasetCEE.txt',dtype='str')
##print(dataset)

muestras_en_cada_media = 22  #Defecto 36#Valor entre 22 y 36
chi_cuadrado = 5     #Defecto 10 #En cuanto vamos a dividir el area bajo la curva
factor_seguridad = 1  #Defecto 70 #Un valor de seguridad que es diferente de 0 que se usa para garantizar que se logre una distribucion normal.

#xxx = catEstadisticalEncoding(dataset,[1,4], muestras_en_cada_media, chi_cuadrado, factor_seguridad)
##print("-------Resultado Final de las Correlaciones -------------")
##print(pearson_correlations(xxx, 0))
#extraerArrayAmaradoxValor(dataset,1,'SS')
""""
#print(np.array([[1,2,3,4,5],[2,4,5,6,8]]))
x = np.array([[1,2,3,4,5],[2,4,5,6,8]])
xc=pearson_correlations(x,0)
#print(xc)
#print(xc[0][1])
"""

