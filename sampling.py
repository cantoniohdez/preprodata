import numpy as np
import scipy as sc


#Generate los valores de las tuplas con remplazo
def simple_random_sampling_with_replacement(size_sample, size_population):
    list = np.random.choice(int(size_population), int(size_sample), replace = True)
    return list

#Generate los valores de las tuplas sin remplazo
def simple_random_sampling_without_replacement(size_sample, size_population):
    list = np.random.choice(int(size_population), int(size_sample), replace = False)
    return list


#Obtener una muestra de tamaño (len(element_sample)) de un dataset. Element_sample contiene el valor de las tuplas que se seleccionan
def obtain_sample(dataset, element_sample):
    newdataset = []
    for x in element_sample:
        newdataset.append(dataset[x])
    return np.array(newdataset)

############################################################################################
#Determinar el tamaño de la muestra
#El método consiste en tratar cada atributo t a obtener una muestra M t , de la siguiente manera.
#Inicialmente M t está vacío. Luego procedemos a extraer al azar (uniforme) elementos seleccionados
#de la población para el atributo t iterativamente y agregando estos elementos a M t .
#En cada iteración i, la entropía se calcula y se compara con la de la iteración anterior.
#ΔH ( i ) = H ( i ) -H ( i-1 )
#Si ΔH ( i ) se acerca a un umbral Epsilon (0.0001) definido el proceso a terminado y ese es el tamaño de la muestra
#para ese atributo y asi se hace para todas las variables y nos quedamos con el mayor de todos los atributos


def calculate_size_sample_using_entropy(dataset, epsilon=0.1):
    size_population = dataset.shape[0]
    list_entropy = sc.stats.entropy(dataset.astype(float), qk=None, base=2)
    list_final_result = []
    count_final = 0
    for var in range(0, dataset.shape[1]):
        list_temp = []
        result = []
        tuples_selected = simple_random_sampling_without_replacement(size_population,size_population)
        count = 0
        for tuple in tuples_selected:
            list_temp.append(dataset[tuple,var])
            entropy = sc.stats.entropy(np.array(list_temp).astype(float), qk=None, base=2)

            if float(list_entropy[var])-float(entropy) < float(epsilon):
                result.append(list_entropy[var])
                result.append(entropy)
                result.append(count)
                list_final_result.append(result)
                if count>count_final:
                    count_final = count

                break
            else:
                count+=1
    #print(count_final)
    #print(list_final_result)
    return int(count_final), list_final_result




