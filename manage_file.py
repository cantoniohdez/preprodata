import csv
import numpy as np

def loadCSV(archivo):
	lines = csv.reader(open(archivo, 'r'))
	datos = list(lines)
	for i in range(len(datos)):
		try:
			datos[i] = [float(x) for x in datos[i]]
		except Exception:
			#print("Entro a la Exception")
			datos[i] = [str(x) for x in datos[i]]
	return np.array(datos)


def savefile(dir_save, dataset):
	temp = dir_save.split(".")
	if str(temp[-1]) == "txt":
		np.savetxt(dir_save, dataset, delimiter='\t', fmt="%s")
	else:
		np.savetxt(dir_save + str(".txt"), dataset, delimiter='\t', fmt="%s")