from PyQt4.QtGui import *
from PyQt4 import QtCore, QtGui
import numpy as np
import random
import scipy as sc
from scipy.stats import chi2_contingency
import sampling
import math

import manage_database
import matplotlib.pyplot as plt
from scipy.interpolate import InterpolatedUnivariateSpline

# Cuenta la cantidad e valores perdidos y me devuelve
# temp : Cantidad de valores perdidos
# dataTemp: Los datos con el valor NaN remplazado por un numero aleatorio para el posterior calculo de Entropia
def count_missing_values(data):
    temp = 0
    newData = []
    for x in range(0, data.shape[0]):
        dataTemp = []
        for y in range(0, data.shape[1]):
            if(str(data[x,y]) == 'NaN'):
                temp+=1
                dataTemp.append(random.random())
            else:
                dataTemp.append(str(data[x,y]))
        newData.append(dataTemp)
    return temp, np.array(newData)

#Calcula la entropia dado un dataset
def calculate_entropy(data):
    list_entropy = sc.stats.entropy(data.astype(float), qk=None, base = 2)
    #print("List Entropia {0}".format(list_entropy))
    entropy = sum(list_entropy)
    return entropy


def calculate_entropy2(data):
    newData = data.T
    list_entropy = []
    for x in newData:
        #print("XXXXX")
        #print(x)
        #print(len(x))
        alphabet = list(set(x))  # list of symbols in the string
        #print('Alphabet of symbols in the string:')
        #print(alphabet)
        # calculate the frequency of each symbol in the string
        freqList = []
        for symbol in alphabet:
            ctr = 0
            for sym in x:
                if sym == symbol:
                    ctr += 1
            freqList.append(float(ctr) / len(x))
        #print('Frequencies of alphabet symbols:')
        #print(freqList)

        # Shannon entropy
        ent = 0.0
        for freq in freqList:
            ent = ent + freq * math.log(freq, 2)
        ent = -ent
        #print('Shannon entropy:')
        #print(ent)
        list_entropy.append(ent)
        #print('Minimum number of bits required to encode each symbol:')
        #print(int(math.ceil(ent)))
    print("Lista de entopias")
    print(list_entropy)
    return sum(list_entropy)


#Elimina las tuplas que contiene valores NaN y devuelve:
#NewDataEnd: en dataset con las tuplas con NAN eliminadas
#Entropy: Entropia del nuevo dataset obtenido
def delete_row_with_missing_value(data):
    newData = []
    for x in range(0, data.shape[0]):
        dataTemp = []
        for y in range(0, data.shape[1]):
            if(str(data[x,y])== 'NaN'):
                dataTemp = []
                break
            else:
                dataTemp.append(str(data[x,y]))
        if len(dataTemp)!=0:
            newData.append(dataTemp)
    newDataEnd = np.array(newData)
    return newDataEnd, calculate_entropy2(newDataEnd)


def msg(text):
    msg = QMessageBox()
    msg.setIcon(QMessageBox.Information)
    msg.setText(text)
    #msg.setInformativeText("This is additional information")
    msg.setWindowTitle("Information")
    msg.setDetailedText("The details are as follows:")
    msg.setStandardButtons(QMessageBox.Ok)
    msg.exec_()

#######################################################################################
#######################################################################################
#######################################################################################
#Imputation a value using Spline Natural
def imputation_value_spline(dataset):
    column_and_tuples_with_mv = column_with_missing_value(dataset)
    for x in column_and_tuples_with_mv.keys():

        #Recorremos las tuplas para eliminar las que tienen valores perdidios
        # Y obtener el valor de las X y de las Y para realizar la interpolacion.
        #Se excluyen los valores perdidos
        column = dataset[:, int(x)]
        #print(column)
        #print(column_and_tuples_with_mv[x])
        axisX , axisY = obtain_values_x_and_y(column)
        spl = InterpolatedUnivariateSpline(axisX, axisY)
        for tuple in column_and_tuples_with_mv[x]:
            #print("Valor en Columna {0} tupla {1}".format(x, tuple))
            #print(spl.__call__(tuple))
            dataset[int(tuple),int(x)] = spl.__call__(tuple)
        #print("DAtasettttt")
        #print(dataset)

    return dataset


#Devuelve los valores de las X y las Y
def obtain_values_x_and_y (column):
    x = []
    y = []
    mv = []
    for i in range(0, column.size):
        if column[i] =="NaN":
            mv.append(i)
        else:
            x.append(i)
            y.append(float(column[i]))

    return np.array(x), np.array(y)




#El metodo devuelve un dictionario con las columnas que contienen valores perdidos y las tuplas de esas columnas que
# contienen esos valores perdidos
def column_with_missing_value (dataset):
    column_numeric = manage_database.obtain_numeric_columns()
    d = {}
    for x in column_numeric:
        list_tuple = []
        flag = False
        for y in range(0, np.array(dataset[:,int(x)]).size):
            if dataset[y,int(x)] == "NaN":
                list_tuple.append(y)
                flag = True
        if flag == True:
            d[x] = list_tuple

    return d


########################################################################################
########################################################################################
########################################################################################
#Chi Square Test
def chi_square_test(dataset, var1, var2):
    try:
        data = np.array(dataset[:,[var1, var2]]).astype(float)
        chi2, p, dof, ex = chi2_contingency(data)
        #print("Chi-Square: {0}".format(chi2))
        #print("Value of P: {0}".format(p))
        #print("Degrees of freedom: {0}".format(dof))

        return chi2 , p ,dof , ex
    except Exception:
        msg(Exception.message)

