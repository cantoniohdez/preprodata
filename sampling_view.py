# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'sampling_view.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui
import sampling
import general_data_views
import manage_database
import complementary_functions as cf
import general_data_views
import numpy as np
import resume


try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_FormSampling(object):

    def __init__(self):
        self.dataset = manage_database.obtain_dataset()
        self.newDataset = np.array([])

    def setupUi(self, Form):
        Form.setObjectName(_fromUtf8("Form"))
        Form.resize(453, 279)
        self.frame = QtGui.QFrame(Form)
        self.frame.setGeometry(QtCore.QRect(10, 10, 211, 201))
        self.frame.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtGui.QFrame.Raised)
        self.frame.setObjectName(_fromUtf8("frame"))
        self.label = QtGui.QLabel(self.frame)
        self.label.setGeometry(QtCore.QRect(10, 10, 201, 17))
        self.label.setObjectName(_fromUtf8("label"))
        self.radioButton = QtGui.QRadioButton(self.frame)
        self.radioButton.setGeometry(QtCore.QRect(10, 40, 191, 22))
        self.radioButton.setObjectName(_fromUtf8("radioButton"))
        #self.radioButton_2 = QtGui.QRadioButton(self.frame)
        #self.radioButton_2.setGeometry(QtCore.QRect(10, 65, 181, 22))
        #self.radioButton_2.setObjectName(_fromUtf8("radioButton_2"))
        self.radioButton_4 = QtGui.QRadioButton(self.frame)
        self.radioButton_4.setGeometry(QtCore.QRect(10, 90, 117, 22))
        self.radioButton_4.setText(_fromUtf8(""))
        self.radioButton_4.setObjectName(_fromUtf8("radioButton_4"))
        self.lineEdit = QtGui.QLineEdit(self.frame)
        self.lineEdit.setGeometry(QtCore.QRect(34, 90, 91, 22))
        self.lineEdit.setObjectName(_fromUtf8("lineEdit"))
        self.label_4 = QtGui.QLabel(self.frame)
        self.label_4.setGeometry(QtCore.QRect(10, 130, 201, 17))
        self.label_4.setObjectName(_fromUtf8("label_4"))
        self.pushButton_5 = QtGui.QPushButton(self.frame)
        self.pushButton_5.setGeometry(QtCore.QRect(130, 160, 71, 27))
        self.pushButton_5.setObjectName(_fromUtf8("pushButton_5"))
        self.pushButton_5.setVisible(False)
        self.pushButton_5.clicked.connect(self.show_resumen)
        #self.pushButton_6 = QtGui.QPushButton(self.frame)
        #self.pushButton_6.setGeometry(QtCore.QRect(10, 160, 99, 27))
        #self.pushButton_6.setObjectName(_fromUtf8("pushButton_6"))
        #self.pushButton_6.setVisible(False)
        self.frame_2 = QtGui.QFrame(Form)
        self.frame_2.setGeometry(QtCore.QRect(230, 10, 211, 201))
        self.frame_2.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame_2.setFrameShadow(QtGui.QFrame.Raised)
        self.frame_2.setObjectName(_fromUtf8("frame_2"))
        self.label_2 = QtGui.QLabel(self.frame_2)
        self.label_2.setGeometry(QtCore.QRect(10, 10, 141, 17))
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.radioButton_5 = QtGui.QRadioButton(self.frame_2)
        self.radioButton_5.setGeometry(QtCore.QRect(10, 60, 191, 21))
        self.radioButton_5.setObjectName(_fromUtf8("radioButton_5"))
        self.radioButton_6 = QtGui.QRadioButton(self.frame_2)
        self.radioButton_6.setGeometry(QtCore.QRect(10, 80, 191, 22))
        self.radioButton_6.setObjectName(_fromUtf8("radioButton_6"))
        self.label_3 = QtGui.QLabel(self.frame_2)
        self.label_3.setGeometry(QtCore.QRect(10, 40, 181, 17))
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.pushButton_3 = QtGui.QPushButton(self.frame_2)
        self.pushButton_3.setGeometry(QtCore.QRect(10, 160, 71, 27))
        self.pushButton_3.setObjectName(_fromUtf8("pushButton_3"))
        self.pushButton_4 = QtGui.QPushButton(self.frame_2)
        self.pushButton_4.setGeometry(QtCore.QRect(120, 160, 81, 27))
        self.pushButton_4.setObjectName(_fromUtf8("pushButton_4"))
        self.pushButton_4.clicked.connect(self.obtain_sample)
        self.frame_3 = QtGui.QFrame(Form)
        self.frame_3.setGeometry(QtCore.QRect(10, 220, 431, 51))
        self.frame_3.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame_3.setFrameShadow(QtGui.QFrame.Raised)
        self.frame_3.setObjectName(_fromUtf8("frame_3"))
        self.pushButton = QtGui.QPushButton(self.frame_3)
        self.pushButton.setGeometry(QtCore.QRect(320, 10, 99, 27))
        self.pushButton.setObjectName(_fromUtf8("pushButton"))
        self.pushButton.clicked.connect(self.insert_sample)
        self.pushButton_2 = QtGui.QPushButton(self.frame_3)
        self.pushButton_2.setGeometry(QtCore.QRect(20, 10, 99, 27))
        self.pushButton_2.setObjectName(_fromUtf8("pushButton_2"))
        self.pushButton_2.clicked.connect(Form.close)

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        Form.setWindowTitle(_translate("Form", "Sampling", None))
        self.label.setText(_translate("Form", "Sample size determination:", None))
        self.radioButton.setText(_translate("Form", "Calculate using Entropy", None))
        #self.radioButton_2.setText(_translate("Form", "RadioButton", None))
        self.label_4.setText(_translate("Form", "Size Determinated: ?", None))
        self.pushButton_5.setText(_translate("Form", "Resume", None))
        #self.pushButton_6.setText(_translate("Form", "Validation", None))
        self.label_2.setText(_translate("Form", "Sampling methods:", None))
        self.radioButton_5.setText(_translate("Form", "with replacemnet", None))
        self.radioButton_6.setText(_translate("Form", "without replacement", None))
        self.label_3.setText(_translate("Form", "Simple random sampling", None))
        self.pushButton_3.setText(_translate("Form", "Save", None))
        self.pushButton_4.setText(_translate("Form", "View", None))
        self.pushButton.setText(_translate("Form", "Accept", None))
        self.pushButton_2.setText(_translate("Form", "Close", None))

    def obtain_sample(self):
        size_population = self.dataset.shape[0]
        flag = True
        if self.radioButton.isChecked() and (self.radioButton_5.isChecked() or self.radioButton_6.isChecked()):
            size_sample, self.result_resume = sampling.calculate_size_sample_using_entropy(self.dataset)
            self.pushButton_5.setVisible(True)
            #self.pushButton_6.setVisible(True)
        #elif self.radioButton_2.isChecked():
            #cf.msg("Method no implemented")
        elif self.radioButton_4.isChecked():
            if self.lineEdit.text()=="":
                cf.msg("Enter sample size into input")
            elif int(self.lineEdit.text()) > size_population:
                cf.msg("The size of the sample is greater than the population")
                flag = False
            else:
                size_sample = int(self.lineEdit.text())
                self.pushButton_5.setVisible(False)
        else:
            cf.msg("Select a method for determined the sample size")
            flag = False

        self.label_4.setText(_translate("Form", "Size Determinated: {}".format(size_sample), None))
        list = []

        if flag == True and self.radioButton_5.isChecked():
            #Muestreo con remplazo
            list = sampling.simple_random_sampling_with_replacement(size_sample,size_population)
            self.newDataset = sampling.obtain_sample(self.dataset, list)
            self.open_view_dataset(self.newDataset)
        elif flag == True and self.radioButton_6.isChecked():
            #Muestro sin remplazo
            list = sampling.simple_random_sampling_without_replacement(size_sample,size_population)
            self.newDataset = sampling.obtain_sample(self.dataset, list)
            self.open_view_dataset(self.newDataset)
        else:
            cf.msg("Select a sampling method")


    def open_view_dataset(self, dataset):
        self.FormDataViewsGeneral = QtGui.QWidget()
        self.ui = general_data_views.Ui_FormDataViewsGeneral(dataset)
        self.ui.setupUi(self.FormDataViewsGeneral)
        self.FormDataViewsGeneral.show()


    def show_resumen(self):
        self.FormResume = QtGui.QWidget()
        self.ui = resume.Ui_FormResume(self.result_resume)
        self.ui.setupUi(self.FormResume)
        self.FormResume.show()

    def insert_sample(self):
        if self.newDataset.size == 0:
            cf.msg("Sampling was not executed")
            Form.close()
        else:
            manage_database.insert_dataset(self.newDataset)



if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    Form = QtGui.QWidget()
    ui = Ui_FormSampling()
    ui.setupUi(Form)
    Form.show()
    sys.exit(app.exec_())

